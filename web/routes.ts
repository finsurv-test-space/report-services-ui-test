import {authService} from './app/services/authService/authService';

export default routesConfig;

/** @ngInject */
function routesConfig($stateProvider: angular.ui.IStateProvider, $urlRouterProvider: angular.ui.IUrlRouterProvider, $locationProvider: angular.ILocationProvider) {
  $locationProvider.html5Mode(true).hashPrefix('!');
  $urlRouterProvider.otherwise('/app/dashboard');

  $stateProvider
    .state('login', {
      url: '/login',
      component: 'login'
    })
    .state('errorPage', {
      url: '/error',
      component: 'errorPage'
    })
    .state('app', {
      url: '/app',
      component: 'app',
      abstract: true,
      resolve: {
		isAuth: function(authService: authService)
		{
          // tslint:disable-next-line:typedef
			authService.whoAmI().then(function(res){
				return;
          	});
        }
      }
    })
    .state('app.dashboard', {
      url: '/dashboard',
      component: 'dashboard'
    })
    .state('app.report', {
      url: '/report/:reportSpace/:trnReference/:channel',
      component: 'report',
      abstract: true
    })
    .state({
      name: 'app.report.bopForm',
      url: '/bopForm',
      component: 'bopForm'
    })
    .state({
      name: 'app.dashboardManagement',
      url: '/dashboardManagement',
      component: 'dashboardManagement'
    })
    .state({
      name: 'app.report.upload',
      url: '/upload',
      component: 'upload'
    })
    .state({
      name: 'app.report.verify',
      url: '/verify',
      component: 'verify'
    })
    .state({
      name: 'app.report.confirmation',
      url: '/confirmation',
      component: 'confirmation'
    })
    .state({
      name: 'app.admin',
      url: '/admin',
      component: 'admin',
      abstract: true,
    })
    .state({
      name: 'app.admin.dashboard',
      url: '/search',
      component: 'adminDashboard'
    })
    .state({
      name: 'app.admin.auditTrail',
      url: '/auditTrail/:trnReference',
      component: 'auditTrail',
    })
    .state({
      name: 'app.admin.userManagement',
      url: '/userManagement',
      component: 'userManagement'
    })
    .state({
      name: 'app.admin.userInformation',
      url: '/userManagement/:user',
      component: 'userManagement'
    })
    .state({
      name: 'app.admin.accessSetManagement',
      url: '/accessSetManagement',
      component: 'accessSetManagement'
    })
    .state({
      name: 'app.admin.accessSetInformation',
      url: '/accessSetInformation/:name',
      component: 'accessSetManagement'
    })
    // .state({
    //   name: 'app.admin.adminPage',
    //   url: '/adminPage',
    //   component: 'adminPage'
    // })
    // .state({
    //   name: 'app.admin.adminUserPage',
    //   url: '/adminPage/:name',
    //   component: 'adminPage'
    // })
    ;
}
