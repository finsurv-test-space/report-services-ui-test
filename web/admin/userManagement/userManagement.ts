import * as angular from 'angular';
import {authService} from '../../app/services/authService/authService';
import {newUserService} from '../../app/services/newUserService/newUserService';
import { PropertiesService } from '../../app/services/propertiesService/propertiesService';

class UserManagementController {
	public totalItems = 64;
	public currentPage = 4;
	public maxSize = 5;
	public bigTotalItems = 175;
	public bigCurrentPage = 1;
	public searchText = '';
	public responseStatus = '';
	public toggleMap;
	public userSelected = false;
	public userName;

	private userList;
	private properties;


  constructor(private $stateParams: ng.ui.IStateParamsService,
    private authService: authService,
    private $http: angular.IHttpService,
    private $timeout: angular.ITimeoutService,
    private Upload: angular.angularFileUpload.IUploadService,
		private $mdDialog: angular.material.IDialogService,
		private newUserService: newUserService) {

		var _self = this;
		_self.properties = PropertiesService.getProperties();

		var url = _self.properties.ReportServicesBaseUrl + (
			_self.properties.ReportServicesGetUsers
			.replace('#{search}', (this.searchText === '' ? '' : '&search=' + this.searchText))
		);

    authService.whoAmI().then(function(res: any) {
      // todo: ONLY ALLOW FOR INTERNAL USERS. DISABLE FOR PRODUCER USERS
			$http.get(url).then(response  => {
				let data = response.data as Array<any>;
				_self.userList = response.data;
				_self.responseStatus = response.status.toString();
				_self.toggleMap = _self.userList.reduce(function(memo: any, item: any) {
				memo.push({
					Username: item.Auth.Username
				});
				return memo;
			}, []);

				for (var i = 0; i < _self.toggleMap.length; i++) {
					if (_self.toggleMap[i].Username === $stateParams.user) {
						_self.userSelected = true;
						_self.userName = $stateParams.user;
						break;
					}
				}
			}), (error) => {
				console.log(error);
			};
		});
  };

  public searchUser() {

	var _self = this;

	var url = _self.properties.ReportServicesBaseUrl + (
		_self.properties.ReportServicesGetUsers
		.replace('#{search}', (this.searchText === '' ? '' : '?usernamesFilter=' + this.searchText))
	);

	this.authService.whoAmI().then(function(res: any) {
		// todo: ONLY ALLOW FOR INTERNAL USERS. DISABLE FOR PRODUCER USERS
		_self.$http.get(url).then(response  => {
			let data = response.data as Array<any>;
			_self.userList = response.data;
			_self.responseStatus = response.status.toString();
			_self.toggleMap = _self.userList.reduce(function(memo: any, item: any) {
			  	memo.push({ Username: item.Auth.Username });
			  	return memo;
			}, []);
		})
		, (error) => {
		console.log(error);
		};
	});
  }

  public showDialog(ev: any) {
    var _self = this;
    var confirm: ng.material.IDialogOptions = {
      controller: DialogController,
      controllerAs: 'dialog',
      template: require('./addUser.html'),
      bindToController: true,
      clickOutsideToClose: false,
      fullscreen: true
    };
    this.$mdDialog.show(confirm).then(function() {
        console.log('here');
    }, function(){
      console.log('rejection');
    });
	};

	public setName(name: string) {
		this.newUserService.setName(name);
		this.userSelected = true;
	}

};


class DialogController {
  public userName;
  public password;
  public selectedRights = [];
  public selectedAccess = [];
  public selectedReportLists = [];
  public selectedUserEvents = [];

  public selectedReportSpaceList = [];
  public reportSpaceList;
  public selectedChannelNameList = [];
  public channelNameList;
  public selectedChannels;
  public channelIsValid = true;
  public accessIsValid = true;
  public passwordIsValid = true;
  public passwordErrorMessage;

  public RightsLookup;
  public AccessLookup;
  public ChannelsLookup;
  public ReportListLookup;
  public UserEventsLookup;
  private properties;

	constructor(
		private $mdDialog: angular.material.IDialogService,
		private $http: angular.IHttpService,
		private authService: authService,
		private newUserService: newUserService) {

		var _self = this;
		_self.properties = PropertiesService.getProperties();

		newUserService.getRights().then(function(res: any) {
		_self.RightsLookup = res;
		});
		newUserService.getAccess('').then(function(res: any) {
		_self.AccessLookup = res;
		});
		newUserService.getReportLists().then(function(res: any) {
		_self.ReportListLookup = res;
		});
		newUserService.getUserEvents().then(function(res: any) {
		_self.UserEventsLookup = res;
		});
		newUserService.getReportSpaces().then(function(res: any) {
			_self.reportSpaceList = res;
		});
	}

  public updateList() {
    if (this.selectedReportSpaceList.length === 0) {
      this.selectedReportSpaceList = this.reportSpaceList[0];
      this.selectedChannelNameList = [];
    }

    var _self = this;

		this.newUserService.getChannels(this.selectedReportSpaceList).then(function(res: any) {
			_self.channelNameList = res;
		});

    if (this.selectedChannelNameList === undefined) {
      this.selectedChannels = [];
    } else {
			this.selectedChannels = [];
			this.selectedChannelNameList.map(function(item: any){
				var foundChannel = _self.newUserService.getAllChannels().find(function(i: any) {
					return (i.ChannelName + ' : ' + i.ReportSpace) === item;
					});
					if (foundChannel) {
						_self.selectedChannels.push(foundChannel);
					}
			});
		}
		this.newUserService.getAccess(this.selectedReportSpaceList).then(function(res: any) {
			_self.AccessLookup = res;
			_self.selectedAccess = _self.getAccessNames();
		});

		this.validate();
  }

  public getAccessNames() {

	var access = this.newUserService.getAcceses();
	var accessList = [];

	for (var i = 0; i < access.length; i++) {
		for (var j = 0; j < this.selectedAccess.length; j++) {
			if (this.selectedAccess[j] === access[i].Name) {
				accessList.push(access[i].Name);
			}
		}
	}

	return accessList;
}

  public cancel(value: string = 'cancel') {
		this.$mdDialog.cancel();

		if(value != "cancel") {
			var url = window.location.href;    
			if (url.includes("userManagement/")) {
				let pos = url.lastIndexOf("/");
				let sub = url.substring(0, pos);
				sub = sub.concat('/' + value)
				window.location.href = sub;
			} else{
				url = url.concat('/' + value);
				window.location.href = url;
			}
		} else {
			window.location.reload();
		}
  };

  public add(ev: any) : void {
    var userObject = {
      Auth: {
		Username: this.userName,
		Password: this.password,
        Rights: this.selectedRights,
		Access: this.selectedAccess,
		Active: true
      },
      Channels: this.selectedChannels,
      ReportLists: this.selectedReportLists,
      UserEvents: this.selectedUserEvents
    };

    var _self = this;

	var url = this.properties.ReportServicesBaseUrl + this.properties.ReportServicesSetUserUrl;

    this.$http.post(url, JSON.stringify(userObject))
      .then(function(response: any) {
				_self.$mdDialog.cancel();
				_self.showConfirmationDialog(ev);
      }, function(response: any){
		  if (response.data.Message.includes('Password should contain')) {
			  _self.passwordIsValid = false;
			  _self.passwordErrorMessage = response.data.Message;
		  }
      });
  }

  public getAccessReportSpace(access : string): string {
	var accesses = this.newUserService.getAcceses();

	for (var i = 0; i < access.length; i ++) {
		if ((accesses[i].Name) === access) {
			return accesses[i].ReportSpace;
		};
	}

}

  public validate() {
		var channelFlag = true;
		var accessFlag = true;

		for (var reportIndex = 0; reportIndex < this.selectedReportSpaceList.length; reportIndex++) {
			let tempChannelFlag = false;
			let tempAccessFlag = false;
			for (var channelIndex = 0; channelIndex < this.selectedChannelNameList.length; channelIndex++) {
				if (this.selectedChannelNameList[channelIndex].includes(this.selectedReportSpaceList[reportIndex])) {
					tempChannelFlag = true;
				}
			}

			if (!tempChannelFlag) {
				channelFlag = false;
			}

			for (var accessIndex = 0; accessIndex < this.selectedAccess.length; accessIndex++) {
				var tempAccess = this.getAccessReportSpace(this.selectedAccess[accessIndex]);

				if (tempAccess === this.selectedReportSpaceList[reportIndex]) {
					tempAccessFlag = true;
				}
			}

			if (!tempAccessFlag) {
				accessFlag = false;
			}
		}

		if (!channelFlag && !accessFlag) {
			this.channelIsValid = false;
			this.accessIsValid = false;
		} else if (!channelFlag) {
			this.channelIsValid = false;
			this.accessIsValid = true;
		} else if (!accessFlag) {
			this.accessIsValid = false;
			this.channelIsValid = true;
		} else {
			this.accessIsValid = true;
			this.channelIsValid = true;
		}
	}

	public showConfirmationDialog(ev: any) {
		var _self = this;
		var confirm: ng.material.IDialogOptions = {
			controller: DialogController,
			controllerAs: 'savedController',
			template: '<md-dialog style="width:25%;">' +
					'<md-dialog-content>' +
					'<div class="md-dialog-content">' +
					'<div class="md-headline" style="font-size:18px!important;">' +
					'The new user has been created' +
					'</div>' +
					'</div>' +
					'</md-dialog-content>' +

					'<md-dialog-actions layout="row">' +
					'<section layout="row" layout-sm="column" layout-align="end center" layout-wrap>' +
					'<md-button ng-click="savedController.cancel(\'username\')" style="color: white !important" class="md-raised md-primary">OK</md-button>' +
					'</section>' +
					'</md-dialog-actions>' +
					'</md-dialog>',
			bindToController: true,
			clickOutsideToClose: false,
			fullscreen: true
		};
		confirm.template = confirm.template.replace('username', this.userName);
		this.$mdDialog.show(confirm).then(function() {
			console.log('here');
		}, function(){
			console.log('rejection');
		});
	};

}


export const userManagement: angular.IComponentOptions = {
  template: require('./userManagement.html'),
  controller: UserManagementController,
  controllerAs: 'userManagement'
};

