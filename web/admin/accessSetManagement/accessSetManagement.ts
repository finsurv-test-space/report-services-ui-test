import * as angular from 'angular';
import {authService} from '../../app/services/authService/authService';
import { PropertiesService } from '../../app/services/propertiesService/propertiesService';

class AccessSetManagementController {

  public accessSets;

  public totalItems = 64;
  public currentPage = 4;
  public maxSize = 5;
  public bigTotalItems = 175;
  public bigCurrentPage = 1;
  public toggleMap;
  public accessSetSelected = false;
  public name = '';
  public selectedSet;
  public searchText = '';

  public responseStatus = '';
  private properties;

  constructor(private $stateParams: ng.ui.IStateParamsService,
    private authService: authService,
    private $http: angular.IHttpService,
    private $timeout: angular.ITimeoutService,
    private Upload: angular.angularFileUpload.IUploadService,
    private $mdDialog: angular.material.IDialogService
  ) {

    
	  this.properties = PropertiesService.getProperties();
    this.selectedSet = $stateParams.name;
    console.log("Chosen Set: ", this.selectedSet);
	  var url = this.properties.ReportServicesBaseUrl + this.properties.ReportServicesAccessSetsUrlBlank;
    var _self = this;
    authService.whoAmI().then(function(res : any) {
      
      $http.get(url).then(response  => {
        let data = response.data as Array<any>;
        _self.accessSets = data.map(function(item) {
          return item.Name;
        });
        _self.responseStatus = response.status.toString();
        if(_self.selectedSet) {
          _self.accessSetSelected = true;
        }
        _self.toggleMap = _self.accessSets.reduce(function(memo: any, item: any) {
          memo.push({
            AccessSetName: item,
            isToggled: false
          });
          return memo;
        }, []);
      });
    });

  };

  public toggle(AccessSetName: String) {
    this.toggleMap.map(function(item: any) {
      if (item.AccesSetName !== AccessSetName) {
        item.isToggled = false;
      }
    });

    // tslint:disable-next-line:typedef
    var AccessSet = this.toggleMap.find(function(i) {
      return i.AccessSetName === AccessSetName;
    });

    AccessSet.isToggled = !AccessSet.isToggled;
  };

  public isToggled(AccessSetName: String) {
    // tslint:disable-next-line:typedef
    var AccessSet = this.toggleMap.find(function(i) {
      return i.AccessSetName === AccessSetName;
    });
    return AccessSet.isToggled;
  };

  // tslint:disable-next-line:typedef
  public showDialog(ev) {
    var _self = this;
    var confirm: ng.material.IDialogOptions = {
      controller: DialogController,
      controllerAs: 'dialog',
      template: require('./addAccessSet.html'),
      bindToController: true,
      clickOutsideToClose: false,
      fullscreen: true
    };
    this.$mdDialog.show(confirm).then(function() {
        console.log('here');
    }, function(){
      console.log('rejection');
    });
  }

}

class DialogController {

  public reportSpacesLookup = ['BON', 'SARB', 'RBM', 'CBL'];
  public accessTypeLookup = ['Read', 'Write'];
  public criteriaTypeLookup = ['Report', 'Meta', 'System'];

  public name;
  public reportSpace;
  public accessType;

  public selectedAccessCriteria = [];
  public tempType = '';
  public tempName = '';
  public tempValue = '';
  private properties;

  constructor(
    private $mdDialog: angular.material.IDialogService,
    private $http: angular.IHttpService,
    private authService: authService) {
      var _self = this;
    _self.properties = PropertiesService.getProperties();
  }

  // tslint:disable-next-line:typedef
  public deleteAccessSet(type, name, value) {
    // tslint:disable-next-line:typedef
    var foundAccessSet = this.selectedAccessCriteria.find(function(item) {
      return item.Type === type && item.Name === name && item.Value === value;
    });
    var index = this.selectedAccessCriteria.indexOf(foundAccessSet);
    this.selectedAccessCriteria.splice(index);
  };

  public addAccessSet() {
    if (this.tempName.trim() != '' && this.tempType.trim() != '' && this.tempValue.trim() != '')
    {
      this.selectedAccessCriteria.push({
        Type: this.tempType,
        Name: this.tempName,
        Values: [this.tempValue]
      });
      this.tempType = '';
      this.tempName = '';
      this.tempValue = '';
    }
  }

  public cancel(value: string = 'cancel') {
    this.$mdDialog.cancel();

		if(value != "cancel") {
			var url = window.location.href;    
			if (url.includes("accessSetInformation/")) {
				let pos = url.lastIndexOf("/");
				let sub = url.substring(0, pos);
				sub = sub.concat('/' + value)
				window.location.href = sub;
			} else{
				url = url.concat('/' + value);
				window.location.href = url;
			}
		} else {
			window.location.reload();
		}
  }

  public add(ev: any) {
    this.addAccessSet();

    var accessSetObject = {
      AccessType: this.accessType,
      AccessCriteria: this.selectedAccessCriteria,
      Name: this.name,
      ReportSpace: this.reportSpace
    };
    // console.log("Hard Coded: ", accessSetObject);
    var _self = this;
    var url = this.properties.ReportServicesBaseUrl + this.properties.ReportServicesAccessSetAdd;
    this.$http.post(url, angular.toJson(accessSetObject)).then(function(res){
      _self.$mdDialog.cancel();
      _self.showConfirmationDialog(ev);
    }, function(res: any) {
      console.log(res);
    });
  }

  public showConfirmationDialog(ev: any) {
		var _self = this;
		var confirm: ng.material.IDialogOptions = {
			controller: DialogController,
			controllerAs: 'savedControllerAccess',
			template: '<md-dialog style="width:25%;">' +
					'<md-dialog-content>' +
					'<div class="md-dialog-content">' +
					'<div class="md-headline" style="font-size:18px!important;">' +
					'The new Access Set has been created' +
					'</div>' +
					'</div>' +
					'</md-dialog-content>' +

					'<md-dialog-actions layout="row">' +
					'<section layout="row" layout-sm="column" layout-align="end center" layout-wrap>' +
					'<md-button ng-click="savedControllerAccess.cancel(\'name\')" style="color: white !important" class="md-raised md-primary">OK</md-button>' +
					'</section>' +
					'</md-dialog-actions>' +
					'</md-dialog>',
			bindToController: true,
			clickOutsideToClose: false,
			fullscreen: true
		};
		confirm.template = confirm.template.replace('name', this.name);
		this.$mdDialog.show(confirm).then(function() {
			console.log('here');
		}, function(){
			console.log('rejection');
		});
	}
}


export const accessSetManagement: angular.IComponentOptions = {
  template: require('./accessSetManagement.html'),
  controller: AccessSetManagementController,
  controllerAs: 'accessSetManagement'
};

