import { authService } from '../../app/services/authService/authService';
import {searchService} from '../services/searchService/searchService';
import { PropertiesService } from '../../app/services/propertiesService/propertiesService';
import { ReportSpaceService } from '../../app/services/ReportingSpaceService/reportSpaceService';
import { report } from '../../app/report/report';
import { LoaderService } from '../../app/services/LoaderService/loaderService';

class AdminDashboardController implements angular.IController {

  public reportSpace: String;
  public channel: String;
  public reportState: String;
  public currentReportList: String = 'All';
  public reports;
  public searchText = '';
  public toggleMap;
  private properties;
  private responseStatus = '';

  constructor(private authService: authService,
    private $http: angular.IHttpService,
    private searchService: searchService,
    private $state: ng.ui.IStateService,
    private ReportSpaceService: ReportSpaceService,
    private LoaderService: LoaderService) {

    var _self = this;
    this.properties = PropertiesService.getProperties();
    authService.whoAmI().then(function () {
      _self.reportSpace = ReportSpaceService.getReportSpace();
      _self.channel = ReportSpaceService.getChannel();

      if (_self.searchService.getSearchText() !== '' && _self.searchService.getSearchText() !== undefined) {
        _self.searchText = _self.searchService.getSearchText();
        _self.getReports();
      };
    });
  }

  public getReports(): void {
    this.responseStatus = 'searching';
    this.searchService.setSearchText(this.searchText);
	  var _self = this;
	  _self.LoaderService.showLoader();
    var url  = _self.properties.ReportServicesBaseUrl +
      (
          _self.properties.ReportServicesListUrl.replace('#{internal}', ('internal/'))
              .replace('#{list}', this.currentReportList)
              .replace('#{reportSpace}', this.properties.reportSpace ? this.properties.reportSpace : 'SARB')
              .replace('#{channelName}', this.properties.ReportChannel ? this.properties.ReportChannel : 'coreSARB')
              .replace('#{search}', (_self.searchText === '' ? '' : '&search=' + _self.searchText))
      );

    this.$http.get(url).then((response: any) => {
        // tslint:disable-next-line:typedef
        _self.reports = response.data.Rows.reduce(function(memo, item){
          memo.push({
            // tslint:disable-next-line:no-string-literal
            'Reference': item['Reference'],
            'TradeReference':  item['Trade Reference'],
            // tslint:disable-next-line:no-string-literal
            'Status': item['Status'],
            'Value Date': item['Value Date'],
            // tslint:disable-next-line:no-string-literal
            'Currency': item['Currency'],
            // tslint:disable-next-line:no-string-literal
            'Amount': item['Amount'],
            // tslint:disable-next-line:no-string-literal
            'ZAR': item['ZAR']
          });
          return memo;
        }, []);
        // tslint:disable-next-line:typedef
        _self.toggleMap = _self.reports.reduce(function (memo, item) {
          memo.push({
            Reference: item.Reference,
            isToggled: false
          });
          return memo;
        }, []);
	  _self.responseStatus = response.status;
	  _self.LoaderService.hideLoader();
      }, (error) => {
		    _self.LoaderService.hideLoader();
        //_self.$state.transitionTo('errorPage');
      });
  };

  public toggle(Reference: String, Status: String) {
    // tslint:disable-next-line:typedef
    this.toggleMap.map(function(item) {
      if (item.Reference !== Reference) {
        item.isToggled = false;
      }
    });
    // tslint:disable-next-line:typedef
    var ref = this.toggleMap.find(function (i) {
      return i.Reference === Reference;
    });
    if (ref.isToggled) {
      ref.isToggled = false;
    } else {
      ref.isToggled = true;
    }
  };

  public isToggled(Reference: String): Boolean {
    // tslint:disable-next-line:typedef
    var ref = this.toggleMap.find(function (i) {
      return i.Reference === Reference;
    });
    return ref.isToggled;
  };

  // tslint:disable-next-line:typedef
  public getFlagClass(currency) {
    var curr = currency.toLowerCase();
    var lookupArray = [
      'sa', 'zar', 'zaf', 'za', 'uk', 'gbp', 'gb', 'usa', 'usd', 'us',
      'eu', 'eur', 'es', 'aus', 'aud', 'au', 'sw', 'chf', 'ch',
      'can', 'cad', 'ca', 'dnk', 'dkk', 'dk', 'hk', 'hkd', 'hkg',
      'jp', 'jpy', 'jpn', 'mu', 'mur', 'mus', 'nz', 'nzd', 'nzl',
      'no', 'nok', 'nor', 'il', 'ils', 'isr', 'sg', 'sgd', 'sgp',
      'se', 'sek', 'swe', 'cn', 'cny', 'chn', 'bw', 'bwp', 'bwa',
      'sk', 'svk', 'hu', 'huf', 'hun', 'ke', 'kes', 'ken',
      'mw', 'mwk', 'mwi', 'mx', 'mxn', 'mex', 'my', 'myr', 'mys',
      'ae', 'aed', 'are', 'zm', 'zmw', 'zmb', 'th', 'thb', 'tha',
      'pl', 'pln', 'pol', 'tr', 'try', 'tur', 'ugx', 'ug',
      'tzs', 'tz', 'ghs', 'gh', 'ngn', 'ng', 'cu', 'czk'
    ];
    if (lookupArray.indexOf(curr) !== -1) {
      return curr;
    }else {
      return 'blank';
    }
  }

}

export const adminDashboard: angular.IComponentOptions = {
  controller: AdminDashboardController,
  template: require('./dashboard.html'),
  controllerAs: 'adminDashboard'
};
