import {authService} from '../../app/services/authService/authService';
import {newUserService} from '../../app/services/newUserService/newUserService';
import { PropertiesService } from '../../app/services/propertiesService/propertiesService';

class UserInformationController {

	public userName;
	public password;
	public passwordErrorMessage;
	public passwordIsValid = true;
  public selectedRights;
  public selectedAccess;
  public selectedChannels;
  public selectedReportLists;
  public selectedUserEvents;
  public editable = true;
  public channelIsValid = true;
  public accessIsValid = true;
  public active;

	public selectedReportSpaceList = [];
  public reportSpaceList;
  public selectedChannelNameList = [];
  public channelNameList;
  public updatedChannels;

  public RightsLookup;
  public AccessLookup;
  public ChannelsLookup;
  public ReportListLookup;
  public UserEventsLookup;
  private properties;


	constructor(private $stateParams: ng.ui.IStateParamsService,
		private authService: authService,
		private $mdDialog: angular.material.IDialogService,
		private $http: angular.IHttpService,
		private $timeout: angular.ITimeoutService,
		private Upload: angular.angularFileUpload.IUploadService ,
		private newUserService: newUserService,
		) {
			var _self = this;
			_self.properties = PropertiesService.getProperties();

			newUserService.getRights().then(function(res: any){
				_self.RightsLookup = res;
			});
			newUserService.getReportLists().then(function(res: any){
				_self.ReportListLookup = res;
			});
			newUserService.getUserEvents().then(function(res: any){
				_self.UserEventsLookup = res;
			});
			newUserService.getReportSpaces().then(function(res: any) {
				_self.reportSpaceList = res;
				newUserService.getChannels(res).then(function(res2: any) {
					_self.channelNameList = res2;
				});
				newUserService.getAccess(res).then(function(res3: any){
					_self.AccessLookup = res3;
				});
			});

			authService.whoAmI().then(function(res: any) {
				_self.userName = $stateParams.user;
				// _self.userName = _self.newUserService.getName();
				var url = _self.properties.ReportServicesBaseUrl + (
					_self.properties.ReportServicesWhoIsUrl.replace('#{username}', _self.userName)
				);

				$http.get(url).then(function(response: any) {

					_self.active = response.data.Auth.Active;
					_self.selectedRights = response.data.Auth.Rights;
					_self.selectedAccess = response.data.Auth.Access;
					_self.selectedChannels = response.data.Channels;
					_self.updatedChannels = _self.selectedChannels;

					_self.selectedReportSpaceList = _self.selectedChannels.reduce(function(memo: any, item: any) {
						if (memo.indexOf(item.ReportSpace) === -1) {
							memo.push(item.ReportSpace);
						}
						return memo;
					}, []);
					_self.selectedChannelNameList = _self.selectedChannels.reduce(function(memo: any, item: any){
						memo.push(item.ChannelName + ' : ' + item.ReportSpace);
						return memo;
					}, []);

					_self.selectedReportLists = response.data.ReportLists;
					_self.selectedUserEvents = response.data.UserEvents;
			}, function(response: any){
				console.log(response);
			});
		});
	};

	public ChannelsAreValid() {
		return this.channelIsValid && this.accessIsValid;
	}

	public disableUser(ev: any) {

		var _self = this;

		var userObject = {
			Auth: {
				Username: this.userName,
				Rights: this.selectedRights,
				Access: this.selectedAccess,
				Active: false
			},
			Channels: this.updatedChannels,
			ReportLists: this.selectedReportLists,
			UserEvents: this.selectedUserEvents
			};

			var url = this.properties.ReportServicesBaseUrl + this.properties.ReportServicesSetUserUrl;

			this.$http.post(url, JSON.stringify(userObject))
			.then(function(response: any){
				console.log(response);
				_self.showConfirmationDialog(ev, "The user has been disabled");
			}, function(response: any){
				console.log(response);
			});
	}
	
	public activateUser(ev: any) {
		var _self = this;
		var userObject = {
			Auth: {
				Username: this.userName,
				Rights: this.selectedRights,
				Access: this.selectedAccess,
				Active: true
			},
			Channels: this.updatedChannels,
			ReportLists: this.selectedReportLists,
			UserEvents: this.selectedUserEvents
			};

			var url = this.properties.ReportServicesBaseUrl + this.properties.ReportServicesSetUserUrl;

			this.$http.post(url, JSON.stringify(userObject))
			.then(function(response: any){
				console.log(response);
				_self.showConfirmationDialog(ev, "The user has been activated");
			}, function(response: any){
				console.log(response);
			});
	}

	public updateList() {
    if (this.selectedReportSpaceList.length === 0) {
      this.selectedReportSpaceList = this.reportSpaceList[0];
      this.selectedChannelNameList = [];
    }

    var _self = this;

		this.newUserService.getChannels(this.selectedReportSpaceList).then(function(res: any) {
			_self.channelNameList = res;
		});

    if (this.selectedChannelNameList === undefined) {
      this.selectedChannels = [];
    } else {
			this.selectedChannels = [];
			this.selectedChannelNameList.map(function(item: any){
				var foundChannel = _self.newUserService.getAllChannels().find(function(i: any) {
					return (i.ChannelName + ' : ' + i.ReportSpace) === item;
					});
					if (foundChannel) {
						_self.selectedChannels.push(foundChannel);
					}
			});
		}
		this.newUserService.getAccess(this.selectedReportSpaceList).then(function(res: any) {
			_self.AccessLookup = res;
			_self.selectedAccess = _self.getAccessNames();
		});

		this.validate();
  }

	public updateSelected() {
		var _self = this;
		var foundFlag = false;

		for (var selChannel = 0; selChannel < this.selectedChannelNameList.length; selChannel++) {
			for (var selReport = 0; selReport < this.selectedReportSpaceList.length; selReport++) {

				if (this.selectedChannelNameList[selChannel].includes(this.selectedReportSpaceList[selReport])) {
					foundFlag = true;
					break;
				}
			}

			if (!foundFlag) {
				_self.selectedChannelNameList.splice(+selChannel, 1);
			}

			foundFlag = false;
		}
	}

	public getAccessNames() {

		var access = this.newUserService.getAcceses();
		var accessList = [];

		for (var i = 0; i < access.length; i++) {
			for (var j = 0; j < this.selectedAccess.length; j++) {
				if (this.selectedAccess[j] === access[i].Name) {
					accessList.push(access[i].Name);
				}
			}
		}

		return accessList;
	}

	public getAccessReportSpace(access : string): string {
		var accesses = this.newUserService.getAcceses();
	
		for (var i = 0; i < accesses.length; i ++) {
			if ((accesses[i].Name) === access) {
				return accesses[i].ReportSpace;
			};
		}
	}

	public validate() {
		var channelFlag = true;
		var accessFlag = true;

		for (var reportIndex = 0; reportIndex < this.selectedReportSpaceList.length; reportIndex++) {
			let tempChannelFlag = false;
			let tempAccessFlag = false;
			for (var channelIndex = 0; channelIndex < this.selectedChannelNameList.length; channelIndex++) {
				if (this.selectedChannelNameList[channelIndex].includes(this.selectedReportSpaceList[reportIndex])) {
					tempChannelFlag = true;
				}
			}

			if (!tempChannelFlag) {
				channelFlag = false;
			}

			for (var accessIndex = 0; accessIndex < this.selectedAccess.length; accessIndex++) {
				var tempAccess = this.getAccessReportSpace(this.selectedAccess[accessIndex]);

				if (tempAccess === this.selectedReportSpaceList[reportIndex]) {
					tempAccessFlag = true;
				}
			}

			if (!tempAccessFlag) {
				accessFlag = false;
			}
		}

		if (!channelFlag && !accessFlag) {
			this.channelIsValid = false;
			this.accessIsValid = false;
		} else if (!channelFlag) {
			this.channelIsValid = false;
			this.accessIsValid = true;
		} else if (!accessFlag) {
			this.accessIsValid = false;
			this.channelIsValid = true;
		}
		else {
			this.accessIsValid = true;
			this.channelIsValid = true;
		}
	}

	public submit(ev: any) {
		// debugger;
		var _self = this;

		var userObject = {
		Auth: {
			Username: this.userName,
			Password: this.password,
			Rights: this.selectedRights,
			Access: this.selectedAccess,
			Active: this.active
		},
		Channels: this.updatedChannels,
		ReportLists: this.selectedReportLists,
		UserEvents: this.selectedUserEvents
		};
		var url = this.properties.ReportServicesBaseUrl + this.properties.ReportServicesSetUserUrl;

		this.$http.post(url, JSON.stringify(userObject))
		.then(function(response: any){
			console.log(response);
			_self.showConfirmationDialog(ev, "The changes to the user were saved");
		}, function(response: any){
			if (response.data.Message.includes("Password should contain")) {
			  _self.passwordIsValid = false;
			  _self.passwordErrorMessage = response.data.Message;
		  }
        console.log(response);
		});
	};

	public cancel() {
		this.$mdDialog.cancel();
		window.location.reload();
  };

	public showConfirmationDialog(ev: any, message: String) {
		var _self = this;
		var confirm: ng.material.IDialogOptions = {
			controller: UserInformationController,
			controllerAs: 'savedEditController',
			template: '<md-dialog style="width:25%;">' +
					'<md-dialog-content>' +
					'<div class="md-dialog-content">' +
					'<div class="md-headline" style="font-size:18px!important;">' +
					message +
					'</div>' +
					'</div>' +
					'</md-dialog-content>' +

					'<md-dialog-actions layout="row">' +
					'<section layout="row" layout-sm="column" layout-align="end center" layout-wrap>' +
					'<md-button ng-click="savedEditController.cancel()" style="color: white !important" class="md-raised md-primary">OK</md-button>' +
					'</section>' +
					'</md-dialog-actions>' +
					'</md-dialog>',
			bindToController: true,
			clickOutsideToClose: false,
			fullscreen: true
		};
		this.$mdDialog.show(confirm).then(function() {
			console.log('here');
		}, function(){
			console.log('rejection');
		});
	}
}


export const userInformation: angular.IComponentOptions = {
  template: require('./userInformation.html'),
  controller: UserInformationController,
  controllerAs: 'userInformation'
};

