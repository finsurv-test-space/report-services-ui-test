import { authService } from '../../app/services/authService/authService';
import { PropertiesService } from '../../app/services/propertiesService/propertiesService';
import { ReportSpaceService } from '../../app/services/ReportingSpaceService/reportSpaceService';

class AuditTrailController {
  public properties;
  public trnReference;
  public reportDiffs;
  public reportDiffsLength;
  public currentVersionNumber;
  public selectedReportVersion;

  constructor(private authService: authService,
    private $http: angular.IHttpService,
	private $stateParams: ng.ui.IStateParamsService,
	private ReportSpaceService: ReportSpaceService) {
      this.properties =  PropertiesService.getProperties();
      this.trnReference = $stateParams.trnReference;
      var url = this.properties.ReportServicesBaseUrl +
          (
              this.properties.ReportServicesAuditTrailUrl
                  .replace('#{internal}', (this.properties.isInternal ? 'internal/' : ''))
                  .replace('#{reportSpace}', this.ReportSpaceService.getReportSpace() ? this.ReportSpaceService.getReportSpace() : 'SARB')
                  .replace('#{channelName}', this.ReportSpaceService.getChannel() ? this.ReportSpaceService.getChannel() : 'coreSARB')
                  .replace('#{trnReference}', this.properties.trnReference ? this.properties.trnReference : this.trnReference.replace(/#/g, '%23'))
          );

      this.$http.get(url)
        .then((res: any) => {
          this.reportDiffs = res.data.ChangeSets;
          this.reportDiffsLength = this.reportDiffs.length;
          this.currentVersionNumber = this.reportDiffsLength;
          this.selectReportVersion();
        });
    }

    public selectReportVersion() {
      this.selectedReportVersion = this.reportDiffs[this.currentVersionNumber - 1];
      var str = JSON.stringify(this.selectedReportVersion.ReportData, undefined, 4);

      this.output(this.syntaxHighlight(str));

    }

    // tslint:disable-next-line:typedef
    public output(inp) {
      var jsoncontainer =  document.getElementById('jsoncontainer');
      var jsoncontainerChildren = jsoncontainer.childNodes;
      if (jsoncontainerChildren[0]) {
        var newNode = document.getElementById('jsoncontainer').appendChild(document.createElement('pre'));
        newNode.innerHTML = inp;
        jsoncontainer.replaceChild(newNode, jsoncontainerChildren[0]);
      }else {
        jsoncontainer.appendChild(document.createElement('pre')).innerHTML = inp;
      }
    }

    // tslint:disable-next-line:typedef
    public syntaxHighlight(json) {
        json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
        // tslint:disable-next-line:typedef
        return json.replace(/("[^"]+"\s*:\s*(?:\b(?:true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?|"[^"]+"))/g, function (match) {
          var cls = '';
          var out = match;
          if (match.indexOf('+') === 1) {
            cls = 'add';
            out = ' + "' + match.substr(2, match.length);
          }else if (match.indexOf('-') === 1) {
            cls = 'delete';
            out = ' - "' + match.substr(2, match.length);
          }
          return '<span class="' + cls + '">' + out + '</span>';
        });
    }




    // tslint:disable-next-line:typedef
    public isArray(a) {
      return (!!a) && (a.constructor === Array);
    };

    // tslint:disable-next-line:typedef
    public isObject(a) {
      return (!!a) && (a.constructor === Object);
    };

    // tslint:disable-next-line:typedef
    public isPrimitive(a) {
      return (a !== Object(a));
    };
}

export const auditTrail: angular.IComponentOptions = {
  template: require('./auditTrail.html'),
  controller: AuditTrailController
};
