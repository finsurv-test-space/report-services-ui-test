import * as angular from 'angular';
import {authService} from '../../app/services/authService/authService';
import { PropertiesService } from '../../app/services/propertiesService/propertiesService';

class AccessSetInformationController {

  public reportSpacesLookup = ['BON', 'SARB', 'RBM', 'CBL'];
  public accessTypeLookup = ['Read', 'Write'];
  public criteriaTypeLookup = ['Report', 'Meta', 'System'];

  public name;
  public reportSpace;
  public accessType;

  public selectedAccessCriteria = [];
  public tempType = '';
  public tempName = '';
  public tempValue = '';

  private properties;

  constructor(private $stateParams: ng.ui.IStateParamsService,
    private authService: authService,
    private $http: angular.IHttpService,
    private $timeout: angular.ITimeoutService,
    private Upload: angular.angularFileUpload.IUploadService,
    private $mdDialog: angular.material.IDialogService
  ) {

    var _self = this;
    _self.properties = PropertiesService.getProperties();
    authService.whoAmI().then(function(res: any) {
      _self.name = $stateParams.name;

      if(_self.name) {
        var url = _self.properties.ReportServicesBaseUrl + (_self.properties.ReportServicesAccessSet.replace('#{access}',  _self.name));
        $http.get(url).then(function(response: any) {
          if (response) {
            // console.log(response)
            _self.reportSpace = response.data.ReportSpace;
            _self.selectedAccessCriteria = response.data.AccessCriteria;
            _self.accessType = response.data.AccessType;
          }
        }), (error) => {
          // console.log(error);
        }
      }
    });
  }

  public deleteAccessSet(type, name, value) {
    var foundAccessSet = this.selectedAccessCriteria.find(function(item: any) {
      return item.Type === type && item.Name === name && item.Value === value;
    });
    var index = this.selectedAccessCriteria.indexOf(foundAccessSet);
    this.selectedAccessCriteria.splice(index);
  }

  public addAccessSet() {
    console.log("tempName: ", this.tempName);
    console.log("tempType: ", this.tempType);
    console.log("tempValue: ", this.tempValue);
    if (this.tempName.trim() != '' && this.tempType.trim() != '' && this.tempValue.trim() != '')
    {
      this.selectedAccessCriteria.push({
        Type: this.tempType,
        Name: this.tempName,
        Values: [this.tempValue]
      });
      this.tempType = '';
      this.tempName = '';
      this.tempValue = '';
    }
  }

  public submit(ev: any) {
    
    this.addAccessSet();
    var AccessCriteria = {
      "AccessType": this.accessType,
      "AccessCriteria": this.selectedAccessCriteria,
      "Name": this.name,
      "ReportSpace": this.reportSpace
    }

    // console.log("Hard Coded: ", AccessCriteria);

    let url = this.properties.ReportServicesBaseUrl + this.properties.ReportServicesAccessSetAdd;
    console.log(angular.toJson(AccessCriteria));
    let _self = this;
    this.$http.post(url, angular.toJson(AccessCriteria)).then(function(res: any) {
      _self.showConfirmationDialog(ev, "The changes to the Access Set were saved");
    }, function(res: any) {
        // console.log(res);
    });
  }

  public showConfirmationDialog(ev: any, message: String) {
		var _self = this;
		var confirm: ng.material.IDialogOptions = {
			controller: AccessSetInformationController,
			controllerAs: 'savedEditController',
			template: '<md-dialog style="width:25%;">' +
					'<md-dialog-content>' +
					'<div class="md-dialog-content">' +
					'<div class="md-headline" style="font-size:18px!important;">' +
					message +
					'</div>' +
					'</div>' +
					'</md-dialog-content>' +

					'<md-dialog-actions layout="row">' +
					'<section layout="row" layout-sm="column" layout-align="end center" layout-wrap>' +
					'<md-button ng-click="savedEditController.cancel()" style="color: white !important" class="md-raised md-primary">OK</md-button>' +
					'</section>' +
					'</md-dialog-actions>' +
					'</md-dialog>',
			bindToController: true,
			clickOutsideToClose: false,
			fullscreen: true
		};
		this.$mdDialog.show(confirm).then(function() {
			console.log('here');
		}, function(){
			console.log('rejection');
		});
	}

  public cancel() {
		this.$mdDialog.cancel();
		window.location.reload();
  };

}

export const accessSetInformation: angular.IComponentOptions = {
  template: require('./accessSetInformation.html'),
  controller: AccessSetInformationController,
  controllerAs: 'accessSetInformation'
};
