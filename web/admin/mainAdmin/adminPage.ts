
class AdminPageController implements angular.IController {
  constructor() {}
}

export const adminPage: angular.IComponentOptions = {
  template: require('./adminPage.html'),
  controller: AdminPageController,
  controllerAs: 'adminPage'
};