import * as angular from 'angular';
import 'angular-ui-router';
import 'angular-ui-bootstrap';

import 'ng-file-upload';
import './css/index.css';
import 'angular-material';
import 'angular-animate';
// tslint:disable-next-line:no-var-requires
require('es6-shim');
require('angular-messages');

import routesConfig from './routes';
import { header } from './app/header';
import { footer } from './app/footer';
import { main } from './app/main';
import { login } from './app/login/login';
import { dashboard } from './app/dashboard/dashboard';
import { report } from './app/report/report';
import { bopForm } from './app/bopform/bopForm';
import { upload } from './app/upload/upload';
import { verify } from './app/verify/verify';
import { confirmation } from './app/confirmation/confirmation';
import { errorPage } from './app/errorPage/errorpage';

import { authService } from './app/services/authService/authService';
import { LoaderService } from './app/services/LoaderService/loaderService';
import { ConfigService } from './app/services/ConfigService/configService';
import { confirmationService } from './app/services/confirmationService/confirmationService';
import { newUserService } from './app/services/newUserService/newUserService';
import { ReportSpaceService } from './app/services/ReportingSpaceService/reportSpaceService';

import { admin } from './admin/admin';
import { adminDashboard } from './admin/dashboard/dashboard';
import { adminPage } from './admin/mainAdmin/adminPage';
import { auditTrail } from './admin/auditTrail/auditTrail';
import { searchService } from './admin/services/searchService/searchService';
import {userManagement} from './admin/userManagement/userManagement';
import {userInformation} from './admin/userInformation/userInformation';
import {accessSetManagement} from './admin/accessSetManagement/accessSetManagement';
import {accessSetInformation} from './admin/accessSetInformation/accessSetInformation';
import { dashboardManagement } from './app/dashboardManagement/dashboardManagement';

import './css/loader.css';
import './css/users.css';
import './css/main.css';


angular
	.module('app', [
		'ui.router',
		'ui.bootstrap',
		'ngFileUpload',
		'ngMaterial',
		'ngMessages'
	])
  // tslint:disable-next-line:typedef
  .config(function ($mdThemingProvider) {
    $mdThemingProvider.theme('default')
      .primaryPalette('blue')
      .accentPalette('grey');
  })
  .config(routesConfig)
  .directive('iframeOnload', function() {
	var directive = {
		scope: {
		  callBack: '&iframeOnload'
		},
		restrict: 'A',
		link: function(scope : any, element : any, attrs : any) {
		  element.on('load', function() {
			var loaderServ = element.injector().get('LoaderService');
			waitSeconds();

			function waitSeconds() {
				setTimeout('', 2000);
				loaderServ.hideLoader();
			}

			return scope.callBack( { element: element[0].src } );
		  });
		}
	  };
	  return directive;
	})
  .component('header', header)
  .component('rsfooter', footer)
  .component('app', main)
  .component('login', login)
  .component('dashboard', dashboard)
  .component('report', report)
  .component('bopForm', bopForm)
  .component('upload', upload)
  .component('verify', verify)
  .component('confirmation', confirmation)
  .component('errorPage', errorPage)
  .component('admin', admin)
  .component('adminPage', adminPage)
  .component('adminDashboard', adminDashboard)
  .component('auditTrail', auditTrail)
  .component('userManagement', userManagement)
  .component('userInformation', userInformation)
  .component('accessSetManagement', accessSetManagement)
  .component('accessSetInformation', accessSetInformation)
  .component('dashboardManagement', dashboardManagement)
  .controller('AppCtrl', function ($scope : any, $mdSidenav : any) {
    $scope.toggleLeft = buildToggler('left');

    function buildToggler(componentId : any) {
      return function() {
        $mdSidenav(componentId).toggle();
      };
    }
  })
  .directive('ngEnter', function () {
    return function (scope: any, element: any, attrs: any) {
      element.bind('keydown keypress', function (event: any) {
          if (event.which === 13) {
            scope.$apply(function () {
                scope.$eval(attrs.ngEnter);
            });
            event.preventDefault();
          }
      });
    };
  })
  .service('searchService', searchService)
  .service('authService', authService)
  .service('LoaderService', LoaderService)
  .service('ConfigService', ConfigService)
  .service('confirmationService', confirmationService)
  .service('newUserService', newUserService)
  .service('ReportSpaceService', ReportSpaceService)
  .run(function($http: angular.IHttpService) {
    // tslint:disable-next-line:no-string-literal
    if (window['access_token']) {
      // tslint:disable-next-line:no-string-literal
      $http.defaults.headers.common.Authorization = 'Bearer ' + (window['access_token']);
	}
  });
