import { authService } from '../services/authService/authService';
import { PropertiesService } from '../services/propertiesService/propertiesService';
import { LoaderService } from '../services/LoaderService/loaderService';

class BOPFormController {
  public trnReference;
  public channel;
  public reportState;
  public formUrl = '';
  public jsUrl = '';

  constructor(private $stateParams: ng.ui.IStateParamsService,
	private $state: ng.ui.IStateService,
	private LoaderService: LoaderService
    // private $stateProvider: ng.ui.IStateProvider
  ) {
	this.LoaderService.showLoader();
    var properties = PropertiesService.getProperties();
    this.trnReference = $stateParams.trnReference;
    this.channel = $stateParams.channel;
    this.reportState = $stateParams.reportState;
    this.formUrl = properties.ReportServicesBaseUrl +
      (
        properties.ReportServicesGetFormUrl
        .replace('#{internal}', (!properties.isInternal ? '' : 'internal/'))
        .replace('#{trnReference}', this.trnReference ? this.trnReference.replace(/#/g, '%23') : '')
        .replace('#{reportSpace}', $stateParams.reportSpace ? this.$stateParams.reportSpace : 'SARB')
        .replace('#{channelName}', this.channel ? this.channel : 'coreSARB')
      );

  }
}

export const bopForm: angular.IComponentOptions = {
  template: require('./bopForm.html'),
  controller: BOPFormController,
  controllerAs: 'bopform'
};
