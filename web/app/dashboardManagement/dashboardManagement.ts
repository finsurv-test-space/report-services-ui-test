import { stringify } from 'querystring';
import { FieldClass, ListClass, RegulatorClass } from '../../app/interfaces/RegulatorFields';
import { authService } from '../../app/services/authService/authService';
import { ConfigService } from '../../app/services/ConfigService/configService';
import { PropertiesService } from '../../app/services/propertiesService/propertiesService';

class DashboardManagementController implements angular.IController {

  public configs: string;
  public regulators: string[] = [];
  public lists: string[] = [];
  private properties;
  private regulatorObject: RegulatorClass[] = [];

  constructor(private ConfigService: ConfigService,  private authService: authService, private $http: angular.IHttpService) {
    var _self = this;
    this.properties = PropertiesService.getProperties();

    authService.whoAmI().then(function () {
      _self.getRegulators();
      _self.configs = _self.ConfigService.getAllConfigs();
      _self.getLists();
    });
  }

  public getRegulators() {
    var url  = this.properties.ReportServicesBaseUrl + (this.properties.ReportServicesReportSpaces.replace('#{internal}', ('internal/')));
		this.$http.get(url).then((response: any) => {
      this.regulators = response.data;
    });
  }

  public getListData() {

    let url  = this.properties.ReportServicesBaseUrl + (this.properties.ReportServicesListsComplete.replace('#{internal}', ('internal/')));
		this.$http.get(url).then((response: any) => {
      for (var regulatorCount in this.regulators) {
        let regObject = new RegulatorClass();
        regObject.regulator = this.regulators[regulatorCount];

        for (var listName in response.data) {
          let list = new ListClass();
          list.listName = listName;

          for (var counter in response.data[listName]['fields']) {
            let field = new FieldClass();
            field.fieldName = response.data[listName]['fields'][counter]['label'];
            list.fields.push(field);
          }

          regObject.lists.push(list);
        }
        this.regulatorObject.push(regObject);
      }
      this.setInitialValues();
    });
  }

  public async setInitialValues() {
    for (var reg in this.regulatorObject) {
      let configData = await this.getConfigDisplay();
      let rowCount = await this.getRowCount(configData, this.regulatorObject[reg].regulator);
      this.regulatorObject[reg].rowCount = rowCount;
      for (var list in this.regulatorObject[reg].lists) {
        let hiddenFields = await this.getFieldDisplay(this.regulatorObject[reg].regulator, this.regulatorObject[reg].lists[list].listName, configData);
        for (var field in this.regulatorObject[reg].lists[list].fields) {
          for (var hiddenField in hiddenFields) {
            if (this.regulatorObject[reg].lists[list].fields[field].fieldName === hiddenFields[hiddenField]) {
              this.regulatorObject[reg].lists[list].fields[field].shouldDisplay = false;
            }
          }
        }
      }
    }
  }

  public async getRowCount(config: any, regulator: string) {
    var data = config['config'];

    for (var regCount in data) {
			if (data[regCount].regulator === regulator) {
        return data[regCount].rowCount ? data[regCount].rowCount : 100;
      }
    }
  }

  public async getConfigDisplay() {
		let url  = this.properties.ReportServicesBaseUrl + (this.properties.ReportServicesColumnConfig.replace('#{internal}', ('internal/'))
		.replace('#{name}', 'rsuiColumnDisplay'));
		let data = await this.$http.get(url).then((response: any) => {
			return response.data;
		});

    return data;
	}

	public async getFieldDisplay(regulator: string, list: string, configData: any) {
		let hiddenColumns: string[] = [];
		// let initialData = await this.getConfigDisplay();
		var data = configData['config'];
		for (var regCount in data) {
			if (data[regCount].regulator === regulator) {
				for (var listCount in data[regCount].lists) {
					if (data[regCount].lists[listCount].listName === list) {
						for (var fieldCounter in data[regCount].lists[listCount].fields) {
							if (data[regCount].lists[listCount].fields[fieldCounter].shouldDisplay === false) {
								hiddenColumns.push(data[regCount].lists[listCount].fields[fieldCounter].fieldName);
							}
						}
					}
				}
			}
		}

		return hiddenColumns;
	}

  public getLists() {
    if (this.lists.length === 0) {
			this.getListData();
		}
  }

  public setConfigFile() {
    this.ConfigService.setConfigFile(this.regulatorObject);
  }
}

export const dashboardManagement: angular.IComponentOptions = {
  template: require('./dashboardManagement.html'),
  controller: DashboardManagementController,
  controllerAs: 'dashboardManagement'
};
