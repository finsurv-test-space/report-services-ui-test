import { confirmationService } from '../services/confirmationService/confirmationService';
import { PropertiesService } from '../services/propertiesService/propertiesService';
import { ReportSpaceService } from '../services/ReportingSpaceService/reportSpaceService';

class ReportController {
	public reportState: String;
	public trnReference: String;
	public channel: String;
	public currentState;

	public completedBopForm;
	public completedDocUpload;
	public completedVerify;
	public saveModal = true;
	public isValid: Boolean;

	private properties;


	constructor(
		private $stateParams: ng.ui.IStateParamsService,
		private $state: ng.ui.IStateService,
		private $http: ng.IHttpService,
		private $rootScope: ng.IRootScopeService,
		private confirmationService: confirmationService,
		private ReportSpaceService: ReportSpaceService,
		private $mdDialog: angular.material.IDialogService
	) {
		this.properties = PropertiesService.getProperties();
		this.currentState = $state.current.name;
		this.reportState = $stateParams.reportState;
		this.trnReference = $stateParams.trnReference;
		this.channel = $stateParams.channel;
		this.checkState();
		this.pollValid();
	};

	// tODO: This is crap, fix!
	pollValid() {
		var _self = this;
		setTimeout(() => {
		// tslint:disable-next-line:no-string-literal
		var valid = window['isValid'] ? window['isValid']() : false;
		if (_self.isValid !== valid) {
			_self.isValid = valid;
			_self.$rootScope.$digest();
		}
		_self.pollValid();
		}, 1000);
	}

	checkState() {
    	// this.currentState = this.$state.current.name;
		this.completedBopForm = false;
		this.completedDocUpload = false;
		this.completedVerify = false;

		if (this.currentState === 'app.report.upload') {
		this.completedBopForm = true;
		}

		if (this.currentState === 'app.report.verify') {
			this.completedBopForm = true;
			this.completedDocUpload = true;
		}

		if (this.currentState === 'app.confirmation') {
		this.completedBopForm = true;
		this.completedDocUpload = true;
		this.completedVerify = true;
		}
  	}

	back() {
		if (this.currentState === 'app.report.bopForm') {
		this.$state.transitionTo('app.dashboard');
		this.currentState = 'app.report.dashboard';
		this.checkState();
		}

		if (this.currentState === 'app.report.upload') {
		this.$state.transitionTo('app.report.bopForm');
		this.currentState = 'app.report.bopForm';
		this.checkState();
		}

		if (this.currentState === 'app.report.verify') {
		this.$state.transitionTo('app.report.upload');
		this.currentState = 'app.report.upload';
		this.checkState();
		}

		if (this.currentState === 'app.confirmation') {
		this.$state.transitionTo('app.report.verify');
		this.currentState = 'app.report.verify';
		this.checkState();
		}
	}

	saveForm(): void {
		// tslint:disable-next-line:no-string-literal
		var data = window['getData']();
		var _self = this;
		var urlStr = _self.properties.ReportServicesBaseUrl + (
			_self.properties.ReportServicesPostReportUrl
			.replace('#{internal}', (_self.properties.isInternal ? 'internal/' : ''))
			.replace('#{schema}', _self.properties.ReportSchema ? _self.properties.ReportSchema : 'sarb')
			.replace('#{channelName}', _self.ReportSpaceService.getChannel() ? _self.ReportSpaceService.getChannel() : 'coreSARB')
		);
		this.$http.post(urlStr, {
			Meta: (data.customData ? data.customData : data.Meta),
			Report: (data.transaction ? data.transaction : data.Report)
		}).then((response) => {
			this.$state.transitionTo('app.report.upload', this.$stateParams);
			this.currentState = 'app.report.upload';
			this.checkState();
		}, (error) => {
			_self.$state.transitionTo('errorPage');
		});
	}

	saveForLater(ev: any): void {
		// tslint:disable-next-line:no-string-literal
		var data = window['getData']();
		var _self = this;
		var urlStr = _self.properties.ReportServicesBaseUrl + (
			_self.properties.ReportServicesPostReportUrl
			.replace('#{internal}', (_self.properties.isInternal ? 'internal/' : ''))
			.replace('#{schema}', _self.properties.ReportSchema ? _self.properties.ReportSchema : 'sarb')
			.replace('#{channelName}', _self.ReportSpaceService.getChannel() ? _self.ReportSpaceService.getChannel() : 'coreSARB')
		);
		this.$http.post(urlStr, {
		Meta: (data.customData ? data.customData : data.Meta),
		Report: (data.transaction ? data.transaction : data.Report)
		}).then((response) => {
			_self.showDialog(ev);
		}, (erro) => {
			_self.$state.transitionTo('errorPage');
		});
	}

	finishedDocs() {
		var _self = this;
		// tslint:disable-next-line:typedef
		this.doUserAction('Confirm').then(function (res) {
		_self.$state.transitionTo('app.report.verify', _self.$stateParams);
		_self.currentState = 'app.report.verify';
		_self.checkState();
		}, (error) => {
		_self.$state.transitionTo('errorPage');
		});
	}

	submit() {
		var _self = this;
		// tslint:disable-next-line:typedef
		this.doUserAction('Approve').then(function (res) {
		_self.$state.transitionTo('app.report.confirmation', _self.$stateParams);
		_self.currentState = 'app.report.confirmation';
		_self.checkState();
		}, (error) => {
		_self.$state.transitionTo('errorPage');
		});
	}

	reject() {
		var _self = this;
		// tslint:disable-next-line:typedef
		this.doUserAction('Reject').then(function (res) {
		_self.$state.transitionTo('app.dashboard', _self.$stateParams);
		}, (error) => {
		_self.$state.transitionTo('errorPage');
		});
	}

	Cancel() {
		this.$mdDialog.cancel();
	}

	Stay() {
		this.Cancel();
		this.$state.transitionTo('app.dashboard', this.$stateParams);
	}

	cancelTransaction() {
		var _self = this;
		// tslint:disable-next-line:typedef
		this.doUserAction('Cancel').then(function (res) {
		_self.$state.transitionTo('app.dashboard', _self.$stateParams);
		}, (error) => {
		_self.$state.transitionTo('errorPage');
		});
	}

	// tslint:disable-next-line:typedef
	doUserAction(action) {
		var endpoint = this.properties.ReportServicesBaseUrl + (
			this.properties.ReportServicesActionUrl
			.replace('#{internal}', (this.properties.isInternal ? 'internal/' : ''))
			.replace('#{trnReference}', this.trnReference.replace(/#/g, '%23') ? this.trnReference.replace(/#/g, '%23') : '')
			.replace('#{reportSpace}', this.properties.ReportSpace ? this.properties.ReportSpace : 'SARB')
			.replace('#{channelName}', this.properties.ReportChannel ? this.properties.ReportChannel : 'coreSARB')
			.replace('#{action}', action ? action : '')
		);

		return this.$http.post(endpoint, {});
	}

	public showDialog(ev: any) {
		var _self = this;
		var confirm: ng.material.IDialogOptions = {
		  controller: ReportController,
		  controllerAs: 'report',
		  template: '<md-dialog style="width:25%;">' +
					'<md-dialog-content>' +
					'<div class="md-dialog-content">' +
					'<div class="md-headline">' +
					'Your Form Has Been Saved' +
					'</div>' +
					'<div class="md-subhead">' +
					'Press "OK" to go back to the dashboard or "Stay" to stay on this page' +
					'</div>' +
					'</div>' +
					'</md-dialog-content>' +

					'<md-dialog-actions layout="row">' +
					'<section layout="row" layout-sm="column" layout-align="end center" layout-wrap>' +
					'<md-button class="md-raised " layout-align="start center" ng-click="report.Cancel()">Stay</md-button>' +
					'<md-button ng-click="report.Stay()" style="color: white !important" class="md-raised md-primary">OK</md-button>' +
					'</section>' +
					'</md-dialog-actions>' +
					'</md-dialog>',
		  bindToController: true,
		  clickOutsideToClose: false,
		  fullscreen: true
		};
		this.$mdDialog.show(confirm).then(function() {
			console.log('here');
		}, function(){
		  console.log('rejection');
		});
	};
}

export const report: angular.IComponentOptions = {
  template: require('./report.html'),
  controller: ReportController,
  controllerAs: 'report'
};
