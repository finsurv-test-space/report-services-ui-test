import 'angular-ui-bootstrap';
import '../../css/login.css';
import { authService } from '../services/authService/authService';
import { LoaderService } from '../services/LoaderService/loaderService';

class LoginController {
	public userName: String = '';
	public passWord: String = '';
	public errorMessage: Boolean = false;

	constructor( private $location: angular.ILocationService,
				private authService: authService,
				private LoaderService: LoaderService) {}

	public login() {
		var _self = this;
		_self.LoaderService.showLoader();

		this.authService.login(this.userName, this.passWord).then(function () {
			if (_self.authService.isAuth()) {
				_self.$location.url('/admin');
				_self.LoaderService.hideLoader();
			} else {
				console.log('Failed');
				_self.errorMessage = true;
				_self.LoaderService.hideLoader();
			}
		});
	}

}

export const login: angular.IComponentOptions = {
  template: require('./login.html'),
  controller: LoginController
// tslint:disable-next-line:eofline
};