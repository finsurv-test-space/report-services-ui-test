class FooterController {
  public currentYear;
  constructor() {
    var d = new Date();
    this.currentYear = d.getFullYear();
  }
}
export const footer: angular.IComponentOptions = {
  template: require('./footer.html'),
  controller: FooterController,
  controllerAs: 'footer'
};
