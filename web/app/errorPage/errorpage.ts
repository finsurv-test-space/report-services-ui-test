
import { authService } from '../services/authService/authService';
import { LoaderService } from '../services/LoaderService/loaderService';

class ErrorPageController implements angular.IController {

  public isAuthorised: Boolean = false;

	constructor(private $location: angular.ILocationService, private $state: angular.ui.IStateService, private LoaderService: LoaderService, private authService: authService) {
    this.LoaderService.hideLoader();
    this.isAuthorised = this.authService.isAuth();
    // _self.isAuthorised = _self.authService.isAuth();
  }

  public ngOnInit(): void {
    this.isAuthorised = this.authService.isAuth();
  }

  	public backToApp() {
      if (this.isAuthorised) {
        this.$location.url('/app');
      } else {
        this.$location.url('/login');
      }
      // go to login...

      // this.$state.transitionTo('login');
    }
}

export const errorPage: angular.IComponentOptions = {
  template: require('./errorPage.html'),
  controller: ErrorPageController,
  controllerAs: 'error'
};
