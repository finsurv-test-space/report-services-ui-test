// tslint:disable-next-line:no-var-requires
let properties = require('../../../properties.json');

export class PropertiesService {

  static getProperties(): any {
    // tslint:disable-next-line:no-string-literal
    return window['BOPProperties'] ? window['BOPProperties'] : properties;
  }

}
