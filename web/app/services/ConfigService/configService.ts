let configs = require('../../../buttonMappings.json');
let columnConfigs = require('../../../hiddenColumns.json');
import { RegulatorClass } from '../../interfaces/RegulatorFields';
import { PropertiesService } from '../propertiesService/propertiesService';

export class ConfigService {
	private properties = PropertiesService.getProperties();
	private initialRegClass: RegulatorClass = new RegulatorClass();
	

	constructor(private $http: angular.IHttpService) { }

	public getConfigs(reportSpace : string, report : any, btnName : string) : boolean {
		if (!configs[reportSpace][btnName]) {
			return false;
		}
		/* tslint:disable:no-string-literal */
		if (configs[reportSpace][btnName]['AllowAll'] === true) {
			return true;
		} else {
			const tempObj = configs[reportSpace][btnName];
			var listConfig = Object.keys(tempObj).map((key) => {
				return { key: key, value: tempObj[key] };
			});

			for (var i = 0; i < listConfig.length; i++) {
				if (listConfig[i].key !== 'AllowAll') {

					if (!report[listConfig[i].key]) {
						return false;
					}

					if (Array.isArray(listConfig[i].value)) {
						if (!(listConfig[i].value).includes(report[listConfig[i].key])) {
							return false;
						}
					} else if (listConfig[i].value.trim() === '' || report[listConfig[i].key].toLowerCase() !== listConfig[i].value.toLowerCase()) {
						return false;
					}
				}
			}

			return true;
		}
	}

	public getAllConfigs() {
		if (!columnConfigs) {
			return [];
		}
		return columnConfigs;
	}

	public getHeaders(reportSpace : string, listType : string, columnName : string) : boolean {
		if (!columnConfigs[reportSpace] || !columnConfigs[reportSpace][listType] || !columnConfigs[reportSpace][listType]['NotDisplayed'])	{
			return true;
		}
		/* tslint:enable:no-string-literal */
		const tempObj = columnConfigs[reportSpace][listType];
		var listConfig = Object.keys(tempObj).map((key) => {
			return { key: key, value: tempObj[key] };
		});

		for (var i = 0; i < listConfig.length; i++) {
			if (Array.isArray(listConfig[i].value)) {
				if ((listConfig[i].value).includes(columnName)) {
					return false;
				}
			} else if (columnName.toLowerCase() === listConfig[i].value.toLowerCase()) {
				return false;
			}
		}
		return true;
	}

	public getPageAmount(reportSpace : string) {
		if (!columnConfigs[reportSpace] || !columnConfigs[reportSpace]['RecordShowLimit']) {
			return 100;
		} else {
			return parseInt(columnConfigs[reportSpace]['RecordShowLimit'], 10);
		}
	}

	public setConfigFile(config: RegulatorClass[]) {
		let url  = this.properties.ReportServicesBaseUrl + (this.properties.ReportServicesColumnConfig.replace('#{internal}', ('internal/'))
			.replace('#{name}', 'rsuiColumnDisplay'));
		this.$http.post(url, {config});
	}
}
