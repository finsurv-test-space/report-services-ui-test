import { PropertiesService } from '../propertiesService/propertiesService';

// tslint:disable-next-line:class-name
export class newUserService {
	private Channels;
	private properties;
	private Access;
	private userName = '';

	constructor(private $http: angular.IHttpService) {
		this.properties = PropertiesService.getProperties();
		this.Channels = [];
	}

	public getRights() {
		// this must only function at an internal-access level!
		// if not internal, we should disable this feature...
		// if (this.properties.isInternal){
			var endpoint = this.properties.ReportServicesBaseUrl + this.properties.ReportServicesRightsUrl
				.replace('#{internal}', (this.properties.isInternal ? 'internal/' : ''));
			return this.$http.get(endpoint).then(function(res: any) {
				return res.data;
			}, function(res: any) {
				console.log(res);
			});
		// } else {
		//   return Promise.resolve([]);
		// }
	};

	public getAcceses() {
		return this.Access;
	}

	public getAccess(reportSpaces : any) {
		// if (this.properties.isInternal){
		var _self = this;
		var endpoint = this.properties.ReportServicesBaseUrl + (
			reportSpaces ? this.properties.ReportServicesAccessSetsUrl.replace('#{reportSpace}', reportSpaces) : 
			this.properties.ReportServicesAccessSetsUrl.replace('?reportSpace=#{reportSpace}', reportSpaces)
		);

		return this.$http.get(endpoint).then(function(res: any) {
			return res.data.reduce(function(memo, item){
				_self.Access = res.data;
				memo.push(item.Name);
				return memo;
			}, []);
		});
		// } else {
		//   return Promise.resolve([]);
		// }
	}

	public getReportSpaces() {

		var endpoint = this.properties.ReportServicesBaseUrl + this.properties.ReportSpaceGetReportSpaces
			.replace('#{internal}', (this.properties.isInternal ? 'internal/' : ''));
			return this.$http.get(endpoint).then(function(res: any) {
				return res.data;
			});
	}

	public setChannels(data : any) {
		this.Channels = data;
	}

	public getChannels(reportSpaces : any) {
		var _self = this;
		var endpoint = this.properties.ReportServicesBaseUrl + (
			this.properties.ReportSpaceGetChannels.replace('#{reportSpace}', reportSpaces)
		);

		return this.$http.get(endpoint).then(function(res: any) {
			_self.setChannels(res.data);
			return res.data.reduce(function(memo: any, item: any){
				  memo.push(item.ChannelName + ' : ' + item.ReportSpace);
				  return memo;
				}, []);
		});
	};

	public getAllChannels() {
		return this.Channels;
	}

	public getReportLists() {
		var endpoint = this.properties.ReportServicesBaseUrl + this.properties.ReportServicesListsUrl
			.replace('#{internal}', (this.properties.isInternal ? 'internal/' : ''));
		return this.$http.get(endpoint).then(function(res: any) {
			return res.data;
		});
	};

	public getUserEvents() {

		var endpoint = this.properties.ReportServicesBaseUrl + this.properties.ReportServicesUserEventsUrl
			.replace('#{internal}', (this.properties.isInternal ? 'internal/' : ''));
		return this.$http.get(endpoint).then(function(res: any) {
			return res.data;
		});
	}

	public setName(name: string) {
    this.userName = name;
		
  }

  public getName() {
    return this.userName;
  }

}
