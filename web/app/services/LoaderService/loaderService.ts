import * as angular from 'angular';

export class LoaderService {

  public showLoader(): void {
    var myElement = angular.element(document.querySelector('#loaderClass'));
    myElement.removeClass('hideLoader');
  }

  public hideLoader(): void {
    var myElement = angular.element(document.querySelector('#loaderClass'));
    myElement.addClass('hideLoader');
  }
}
