export class ReportSpaceService {

	private ReportingDetails = new Map<string, string[]>();
	private currentReportSpace;
	private currentChannel;
	private counter = 0;

	constructor(private $http: angular.IHttpService, private $state: ng.ui.IStateService) { }

	public setReportDetails(channels: String[]) {
		if ((!this.ReportingDetails || this.ReportingDetails.size === 0) && this.counter === 0) {
			this.ReportingDetails = new Map<string, string[]>();
			this.counter = 1;
			for (var i = 0; i < channels.length; i++) {
				/* tslint:disable:no-string-literal */
				if (this.ReportingDetails.has(channels[i]['ReportSpace'])) {
					var tempValue = new Array();
					tempValue = this.ReportingDetails.get(channels[i]['ReportSpace']);
					tempValue.push('' + channels[i]['ChannelName']);
					this.ReportingDetails.set(channels[i]['ReportSpace'], tempValue);
				} else {
					var tempValue = new Array();
					tempValue.push('' + channels[i]['ChannelName']);
					this.ReportingDetails.set(channels[i]['ReportSpace'].toString(), tempValue);
				}
				/* tslint:enable:no-string-literal */
			}

			var initialKey = '';

			for (let key of this.ReportingDetails.keys()) {
				if (initialKey === '') {
					initialKey = key;
					break;
				}
			}

			var initialKeyChan = this.ReportingDetails.get(initialKey);
			this.setReportSpace(initialKey);
			this.setChannel(initialKeyChan[0]);
		}
	}
	public setChannel(newChannel : string) {
		this.currentChannel = newChannel;
	}

	public getChannel() {
		return this.currentChannel;
	}

	public getReportSpace() {
		return this.currentReportSpace;
	}

	public setReportSpace(newReportSpace : string) {
		this.currentReportSpace = newReportSpace;
	}

	public getAllReportSpaces() {
		var reportSpaces : string[] = [];

		for (let key of this.ReportingDetails.keys()) {
			for (let value of this.ReportingDetails.get(key)) {
				reportSpaces.push('' + key + ' : ' + value + '');
			}
		}
		return reportSpaces;
	}
}
