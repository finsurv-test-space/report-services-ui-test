import { PropertiesService } from '../../services/propertiesService/propertiesService';


// tslint:disable-next-line:class-name
export class user {
  private userName: String;
  private rights: String[];
  private access: String[];
  private channelName: String;
  private reportSpace: String;
  private reportList: String[];
  private userEvents: String[];
  private allAllowedActions: any;
  private properties: any;

  constructor(private $http: angular.IHttpService, private $location: angular.ILocationService, private $state: ng.ui.IStateService) {
    this.properties = PropertiesService.getProperties();
  };

  public getUserName(): String {
    return this.userName;
  };
  public setUserName(userName: String): void {
    this.userName = userName;
  };

  public getRights(): String[] {
    return this.rights;
  };
  public setRights(rights: String[]): void {
    this.rights = rights;
  };

  public getAccess(): String[] {
    return this.access;
  };
  public setAccess(access: String[]): void {
    this.access = access;
  };

  public getChannelName(): String {
    return this.channelName;
  };
  public setChannelName(channelName: String): void {
    this.channelName = channelName;
  };

  public getReportSpace(): String {
    return this.reportSpace;
  };
  public setReportSpace(reportSpace: String): void {
    this.reportSpace = reportSpace;
  };

  public getReportList(): String[] {
    return this.reportList;
  };
  public setReportList(reportList: String[]): void {
    this.reportList = reportList;
  };

  public getUserEvents(): String[] {
    return this.userEvents;
  };
  public setUserEvents(userEvents: String[]): void {
    this.userEvents = userEvents;
  };



  public computeAllAllowedActions() {

    var _self = this;

    let reportSpace: String = this.getReportSpace();
    let userEvents: String[] = this.getUserEvents();

    var endpoint = this.properties.ReportServicesBaseUrl + this.properties.ReportServicesReportSpaceUrl
        .replace('#{internal}', (this.properties.isInternal ? 'internal/' : ''))
        .replace('#{reportSpace}', reportSpace ? reportSpace : 'SARB');

    return this.$http.get(endpoint).then(

      function (res: any) {
        var out = {};

        // tslint:disable-next-line:typedef
        res.data.States.map(function (item) {
          if (!out.hasOwnProperty(item.Name)) {
            out[item.Name] = {};
            // tslint:disable-next-line:no-string-literal
            out[item.Name]['allowedActions'] = _self.returnArrayIntersection(item.UserEvents, userEvents);
          }
        });

        return out;

      }, function (error: any) {
        _self.$state.transitionTo('errorPage');
      });
  }

  public setAllAllowedActions(allAllowedActions: String) {
    this.allAllowedActions = allAllowedActions;
  }

  public getAllAllowedActions() {
    return this.allAllowedActions;
  }

  private returnArrayIntersection(arr1: String[], arr2: String[]): String[] {

    // tslint:disable-next-line:typedef
    var out = arr1.reduce(function (memo, item) {
      if (arr2.indexOf(item) !== -1) {
        memo.push(item);
      }
      return memo;
    }, []);

    // tslint:disable-next-line:typedef
    return arr2.reduce(function (memo, item) {
      if (arr1.indexOf(item) !== -1 && memo.indexOf(item) === -1) {
        memo.push(item);
      }
      return memo;
    }, out);

  };

};
