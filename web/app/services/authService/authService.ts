// this service is gets info on all the users. Used for login and UI - the Ui will change according to the permissions the user has.
// the user events will be the interserction between the User Events and actions allowed on the state of a report.

import { user } from './user';
import { PropertiesService } from '../../services/propertiesService/propertiesService';
import { ReportSpaceService } from '../../services/ReportingSpaceService/reportSpaceService';


// tslint:disable-next-line:class-name
export class authService {
  public isAuthorised: Boolean = false;
  public user: user;
  private properties;

	constructor(
		private $http: angular.IHttpService,
		private $location: angular.ILocationService,
		private $state: ng.ui.IStateService,
		private ReportSpaceService: ReportSpaceService) {
		this.properties = PropertiesService.getProperties();
		this.user = new user($http, $location, $state);
	}

  public isAuth() {
    return this.isAuthorised;
  }

  public login(userName: String, password: String) {

    var _self = this;

    // tslint:disable-next-line:typedef
    function success(res) {
		_self.setAuth(true);
    	_self.whoAmI();
    }

    // tslint:disable-next-line:typedef
    function failure(res) {
    	console.log(JSON.stringify(res));
    	// _self.$state.transitionTo('errorPage');
    }
    return this.$http.post(this.properties.ReportServicesBaseUrl + this.properties.ReportServicesLoginUrl, JSON.stringify({ 'Username': userName, 'SetCookie': true,  'Password': password }))
      .then(success, failure);
  }

  public logout() {
    var _self = this;

    // tslint:disable-next-line:typedef
    return this.$http.get(this.properties.ReportServicesBaseUrl + this.properties.ReportServicesLogoutUrl + '?username=' + this.user.getUserName()).then(function (res) {
	  _self.setAuth(false);
	  _self.$state.transitionTo('login');

    }, (error) => {
      _self.$state.transitionTo('errorPage');
    });
  }

  public whoAmI() {
    var _self = this;
    return this.$http.get(this.properties.ReportServicesBaseUrl + this.properties.ReportServicesWhoAmIUrl).then(function (res: any) {
		if (res.data && res.data.Auth && res.data.Auth.Username && res.data.Auth.Username !== 'anonymousUser') {
			_self.setAuth(true);
			_self.user.setUserName(res.data.Auth.Username);
			_self.user.setRights(res.data.Auth.Rights);
			_self.user.setAccess(res.data.Auth.Access);
			_self.user.setChannelName(res.data.Channels[0].ChannelName);
			_self.user.setReportSpace(res.data.Channels[0].ReportSpace);
			_self.user.setReportList(res.data.ReportLists);
			_self.user.setUserEvents(res.data.UserEvents);

			if (!_self.ReportSpaceService.getChannel()) {
				_self.ReportSpaceService.setReportDetails(res.data.Channels);
			}
		} else {
			console.log('Anon User');
			_self.$state.transitionTo('login');
		}
      	return res.data;
    }, (error) => {
      _self.$state.transitionTo('errorPage');
    });
  }

  public hasPermission(authRequired : String) : boolean {
    var permissions = this.user.getRights();

    switch (authRequired) {
      case 'View Users' :
        break;
    }

    return true;

  }

  private setAuth(bool: Boolean) {
	console.log('Auth Set To: ' + bool);
    this.isAuthorised = bool;
  }
}



