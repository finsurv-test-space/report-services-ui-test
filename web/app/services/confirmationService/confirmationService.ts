// tslint:disable-next-line:class-name
export class confirmationService {
  private confirmed: boolean = false;
  private uploaded: boolean = false;

  private confirmImpExp: boolean = undefined;

  public allChecked() {
    return (typeof this.confirmImpExp === 'undefined') ? this.confirmed :
      (this.confirmed && this.confirmImpExp);

  }

  public isConfirmed() {
    return this.confirmed;
  }

  public setConfirmed(bool: boolean) {
    this.confirmed = bool;
  }


  public isConfirmedImpExp() {
    return this.confirmImpExp;
  }

  public setConfirmedImpExp(bool: boolean) {
    this.confirmImpExp = bool;
  }

  public allUploaded() {
    return this.uploaded;
  }

  public setAllUploaded(bool: boolean) {
    this.uploaded = bool;
  }

}
