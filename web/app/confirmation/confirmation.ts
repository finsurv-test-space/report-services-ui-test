import { confirmationService } from '../services/confirmationService/confirmationService';
import { PropertiesService } from '../services/propertiesService/propertiesService';


class ConfirmationClass {

  public baseUrl;
  public trnReference;
  public channel;
  public bopData;
  public documentNames = [];
  public reasons;
  public isInternal;
  public properties;

  constructor(
    private $http: ng.IHttpService,
    private $stateParams: ng.ui.IStateParamsService,
    private $state: ng.ui.IStateService,
    private confirmationService: confirmationService,
  ) {
    this.properties = PropertiesService.getProperties();
    this.baseUrl = this.properties.ReportServicesBaseUrl;
    this.trnReference = $stateParams.trnReference;
    this.channel = $stateParams.channel;
    this.isInternal = this.properties.isInternal;
    var _self = this;
    var getReportUrl = this.properties.ReportServicesBaseUrl +
      (
        this.properties.ReportServicesGetReportUrl
        .replace('#{internal}', (!this.isInternal ? '' : 'internal/'))
        .replace('#{trnReference}', this.trnReference ? this.trnReference.replace(/#/g, '%23') : '')
        .replace('#{reportSpace}', this.$stateParams.reportSpace ? this.$stateParams.reportSpace : 'SARB')
        .replace('#{channelName}', this.channel ? this.channel : 'coreSARB')
      );

    $http
      .get(getReportUrl)
      .then((res: any) => {
        this.bopData = JSON.parse(JSON.stringify(res.data.Report));

        _self.getDocumentList().then(function (res: any) {
          console.log(res.data);
          // tslint:disable-next-line:typedef
          res.data.forEach(function (item) {
            _self.documentNames.push({
              type: item.Type,
              name: item.FileName
            });
          });
          _self.reasons = _self.getMonetoryReasons();
        });
      }, (Error) => {
        $state.transitionTo('errorPage');
      });

  }

  public getMonetoryReasons() {
    var reasons = '';
    // tslint:disable-next-line:typedef
    this.bopData.MonetaryAmount.map(function (item) {
      reasons += item.CategoryCode + ', ';
    });
    return reasons.substr(0, reasons.length - 2);
  }

  public getDocumentList() {
    var getDocsUrl = this.properties.ReportServicesBaseUrl +
      (
        this.properties.ReportServicesGetReportUrl
        .replace('#{internal}', (!this.isInternal ? '' : 'internal/'))
        .replace('#{trnReference}', this.trnReference ? this.trnReference.replace(/#/g, '%23') : '')
        .replace('#{reportSpace}', this.$stateParams.reportSpace ? this.$stateParams.reportSpace : 'SARB')
        .replace('#{channelName}', this.channel ? this.channel : 'coreSARB')
      );
    return this.$http
      .get(getDocsUrl);
  }

}

export const confirmation: angular.IComponentOptions = {
  template: require('./confirmation.html'),
  controller: ConfirmationClass
};
