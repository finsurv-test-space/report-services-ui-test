import { confirmationService } from '../services/confirmationService/confirmationService';
import { PropertiesService } from '../services/propertiesService/propertiesService';


class VerifyController {

  public trnReference;
  public channel;
  public showFullBopForm;
  public bopData;
  public baseUrl;
  public documentNames = [];
  public reasons;
  public code1xx = false;
  public properties;

  // confirmation stuff
  public checkDocumentCheckBox = false;
  public checkImpExp = false;

  constructor(
    private $http: ng.IHttpService,
    private $stateParams: ng.ui.IStateParamsService,
    private $state: ng.ui.IStateService,
    private confirmationService: confirmationService,
    private $uibModal: ng.ui.bootstrap.IModalService
  	) {
		this.properties = PropertiesService.getProperties();
		this.trnReference = $stateParams.trnReference;
		this.channel = $stateParams.channel;
		this.confirmationService.setConfirmed(false);

		this.baseUrl = this.properties.ReportServicesBaseUrl;
		var _self = this;
		var endpoint = this.baseUrl + (
			this.properties.ReportServicesGetReportUrl
			.replace('#{internal}', (this.properties.isInternal ? 'internal/' : ''))
			.replace('#{trnReference}', this.$stateParams.trnReference.replace(/#/g, '%23') ? this.$stateParams.trnReference.replace(/#/g, '%23') : '')
			.replace('#{reportSpace}', this.$stateParams.reportSpace ? this.$stateParams.reportSpace : 'SARB')
			.replace('#{channelName}', this.properties.ReportChannel ? this.properties.ReportChannel : 'coreSARB')
			.replace('#{schema}', this.properties.ReportSchema ? this.properties.ReportSchema : '')
		);
		$http.get(endpoint).then((res: any) => {
			this.bopData = JSON.parse(JSON.stringify(res.data.Report));
			_self.getDocumentList().then(function (res: any) {
			console.log(res.data);
			// tslint:disable-next-line:typedef
			res.data.forEach(function (item) {
				_self.documentNames.push({
				type: item.Type,
				name: item.FileName
				});
			});
			_self.reasons = _self.getMonetoryReasons();
			if (_self.code1xx) {
				_self.confirmationService.setConfirmedImpExp(false);
			}
			}, (error) => {
			$state.transitionTo('errorPage');
			});
		}, (error) => {
			$state.transitionTo('errorPage');
		});
	};


  // tslint:disable-next-line:typedef
  public setConfirmed(opt) {
    this.confirmationService.setConfirmed(opt);
  };

  // tslint:disable-next-line:typedef
  public setConfirmedImpExp(opt) {
    this.confirmationService.setConfirmedImpExp(opt);
  };

  public toggleShowFullBopForm() {
    this.showFullBopForm = !this.showFullBopForm;
  }

  // tslint:disable-next-line:typedef
  public open(ev): void {
    var modalInstance: ng.ui.bootstrap.IModalServiceInstance = this.$uibModal.open({
      template: require('./declaration.html'),
      controller: DeclarationModalController,
      bindToController: true,
      controllerAs: '$ctrl'
    });

  }

  // tslint:disable-next-line:typedef
  public openTandC(ev): void {
    var modalInstance: ng.ui.bootstrap.IModalServiceInstance = this.$uibModal.open({
      template: require('./TandC.html'),
      controller: TandCModalController,
      bindToController: true,
      size: 'lg',
      controllerAs: '$ctrl'
    });
  }

  // tslint:disable-next-line:typedef
  public isArray(a) {
    return (!!a) && (a.constructor === Array);
  };

  // tslint:disable-next-line:typedef
  public isObject(a) {
    return (!!a) && (a.constructor === Object);
  };

  // tslint:disable-next-line:typedef
  public isPrimitive(a) {
    return (a !== Object(a));
  };

	public getDocumentList() {
		var endpoint = this.baseUrl + (
			this.properties.ReportServicesDocumentGetRequiredUrl
			.replace('#{internal}', (this.properties.isInternal ? 'internal/' : ''))
			.replace('#{trnReference}', this.trnReference.replace(/#/g, '%23') ? this.trnReference.replace(/#/g, '%23') : '')
			.replace('#{channelName}', this.properties.ReportChannel ? this.properties.ReportChannel : 'coreSARB')
			.replace('#{schema}', this.properties.ReportSchema ? this.properties.ReportSchema : '')
		);
		return this.$http.get(endpoint);
	}

  public getMonetoryReasons() {
    var reasons = '';
    var _self = this;
    // tslint:disable-next-line:typedef
    this.bopData.MonetaryAmount.map(function (item) {
      if (item.CategoryCode[0] === '1') {
        _self.code1xx = true;
      }
      reasons += item.CategoryCode + ', ';
    });
    return reasons.substr(0, reasons.length - 2);
  }

}


class DeclarationModalController {
  static $inject = ['$uibModalInstance'];
  constructor(private $uibModalInstance: ng.ui.bootstrap.IModalServiceInstance) {
  }

  ok(): void {
    this.$uibModalInstance.close();
  };

  cancel(): void {
    this.$uibModalInstance.dismiss('cancel');
  };
}

class TandCModalController {
  static $inject = ['$uibModalInstance'];
  constructor(private $uibModalInstance: ng.ui.bootstrap.IModalServiceInstance) {
  }

  ok(): void {
    this.$uibModalInstance.close();
  };

  cancel(): void {
    this.$uibModalInstance.dismiss('cancel');
  };
}

export const verify: angular.IComponentOptions = {
  template: require('./verify.html'),
  controller: VerifyController
};
