import { PropertiesService } from './services/propertiesService/propertiesService';
import {ReportSpaceService} from './services/ReportingSpaceService/reportSpaceService';
import { authService } from './services/authService/authService';

class HeaderController {
	public reportSpaces;
	private properties;
  private channels;

  // tslint:disable-next-line:one-line
  	constructor(private authService: authService,
	private $http: angular.IHttpService,
	private $location: angular.ILocationService,
	private $state: ng.ui.IStateService,
	private ReportSpaceService: ReportSpaceService) { }

	public goToDashboard() {
		window.location.href = 'app/dashboard';
	};

	public logout() {
		this.authService.logout().then(function() {
			window.location.href = 'login';
		});
	  }
}

export const header: angular.IComponentOptions = {
  template: require('./header.html'),
  controller: HeaderController,
  controllerAs: 'header'
};
