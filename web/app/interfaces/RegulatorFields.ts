type regulatorTypes = 'regulator' | 'lists' | 'rowCount';
type listTypes = 'listName' | 'fields';
type fieldTypes = 'fieldName' | 'shouldDisplay';

interface IRegulatorInterface {
  regulator: string;
  lists: IListInterface[];
}

interface IListInterface {
  listName: string;
  fields: IFieldInterface[];
}

interface IFieldInterface {
  fieldName: string;
  shouldDisplay: boolean;
}

export class RegulatorClass {
  regulator: string = '';
  lists: ListClass[] = [];
  rowCount: number = 100;

  constructor(regulator?: IRegulatorInterface) {
    this.regulator = this.assignVariable('regulator', regulator, this.regulator);
    this.lists = [...this.assignVariable('lists', regulator, this.lists)];
    this.rowCount = this.assignVariable('rowCount', regulator, this.rowCount);
  }

  private assignVariable(key: regulatorTypes, object: IRegulatorInterface, defaultVal: any) {
    return object ? (object[key] ? object[key] : defaultVal) : defaultVal;
  }
}

export class ListClass {
  listName: string = '';
  fields: FieldClass[] = [];

  constructor(list?: IListInterface) {
    this.listName = this.assignVariable('listName', list, this.listName);
    this.fields = [...this.assignVariable('fields', list, this.fields)];
  }

  private assignVariable(key: listTypes, object: IListInterface, defaultVal: any) {
    return object ? (object[key] ? object[key] : defaultVal) : defaultVal;
  }
}

export class FieldClass {
  fieldName: string = '';
  shouldDisplay: boolean = true;

  constructor(field?: IFieldInterface) {
    this.fieldName = this.assignVariable('fieldName', field, this.fieldName);
    this.shouldDisplay = this.assignVariable('shouldDisplay', field, this.shouldDisplay);
  }

  private assignVariable(key: fieldTypes, object: IFieldInterface, defaultVal: any) {
    return object ? (object[key] ? object[key] : defaultVal) : defaultVal;
  }
}
