import { authService } from '../services/authService/authService';
import { PropertiesService } from '../services/propertiesService/propertiesService';
import { confirmationService } from '../services/confirmationService/confirmationService';

class UpoadController {

    public trnReference;
    public channel;
    public reportSpace;
    public baseUrl;
    public file;
    public allUploaded;

    public errorMsg;
    public documentLink;
    public documentList;
    public fileList = [];
    private properties;

    constructor(
        private $stateParams: ng.ui.IStateParamsService,
        private $state: ng.ui.IStateService,
        private authService: authService,
        private $http: angular.IHttpService,
        private $timeout: angular.ITimeoutService,
        private Upload: angular.angularFileUpload.IUploadService,
        private confirmationService: confirmationService
    ) {
        this.properties = PropertiesService.getProperties();
        this.trnReference = $stateParams.trnReference;
        this.channel = $stateParams.channel;
        this.reportSpace = $stateParams.reportSpace;
        this.baseUrl = this.properties.ReportServicesBaseUrl;

        this.getDocumentList().then((response: any) => {
            var _self = this;
            this.documentList = response.data;
            response.data.forEach(item => {
                var file;
                this.fileList.push(file);
            });
            this.checkAllUploaded();
            this.confirmationService.setAllUploaded(this.allUploaded);

        }, (error) => {
            this.$state.transitionTo('errorPage');
        });
    }

    // tslint:disable-next-line:typedef
    public uploadFile(file, documentHandle) {
        var _self = this;
		var postFileEndpoint = this.properties.ReportServicesBaseUrl + (
			this.properties.ReportServicesDocumentPostUrl
			.replace('#{internal}', (this.properties.isInternal ? 'internal/' : ''))
			.replace('#{trnReference}', this.trnReference.replace(/#/g, '%23') ? this.trnReference.replace(/#/g, '%23') : '')
			.replace('#{reportSpace}', this.properties.ReportSpace ? this.properties.ReportSpace : 'SARB')
			.replace('#{channelName}', this.properties.ReportChannel ? this.properties.ReportChannel : 'coreSARB')
			.replace('#{documentHandle}', documentHandle ? documentHandle : '')
        );
        // _self.baseUrl + 'internal/producer/api/document?reportSpace=' + _self.reportSpace + '&documentHandle=' + documentHandle.replace(/#/g, '%23')


        var getFileEndpoint = this.properties.ReportServicesBaseUrl + (
          this.properties.ReportServicesDocumentGetUrl
          .replace('#{internal}', (this.properties.isInternal ? 'internal/' : ''))
          .replace('#{trnReference}', this.trnReference.replace(/#/g, '%23') ? this.trnReference.replace(/#/g, '%23') : '')
          .replace('#{reportSpace}', this.properties.ReportSpace ? this.properties.ReportSpace : 'SARB')
          .replace('#{channelName}', this.properties.ReportChannel ? this.properties.ReportChannel : 'coreSARB')
          .replace('#{documentHandle}', documentHandle ? documentHandle : '')
        );
        // _self.baseUrl + 'internal/producer/api/document?channelName=invBMP&documentHandle=' + documentHandle.replace(/#/g, '%23')


        if (file !== null) {
            file.upload = _self.Upload.upload({
                method: 'POST',
                url: postFileEndpoint,
                data: { file: file },
                withCredentials: true
            });
            // tslint:disable-next-line:typedef
            file.upload.then(function (response) {
                file.result = response.data;
                file.documentLink = getFileEndpoint;
                _self.getDocumentList().then((res: any) => {

                    _self.documentList = res.data;
                    _self.checkAllUploaded();
                    _self.confirmationService.setAllUploaded(_self.allUploaded);
                }, (error) => {
                    _self.$state.transitionTo('errorPage');
                });
            // tslint:disable-next-line:typedef
            }, function (error) {
                _self.errorMsg = 'Error Type: ' + error.data.ErrorType + ', Error Message: ' + error.data.Message;
                console.log(error.data);
            // tslint:disable-next-line:typedef
            }, function (evt) {
                // math.min is to fix IE which reports 200% sometimes
                file.progress = 100 * evt.loaded / evt.total;
            });
        }
    }

    // tslint:disable-next-line:typedef
    public openFile(filelink) {
        window.open(filelink, '_new');
    }

    public checkAllUploaded() {
        this.allUploaded = true;
        this.documentList.forEach(item => {
            if (!item.Uploaded) {
                this.allUploaded = false;
            }
        });
    }

    public getDocumentList() {

        var getDocsRequiredEndpoint = this.properties.ReportServicesBaseUrl + (
          this.properties.ReportServicesDocumentGetRequiredUrl
          .replace('#{internal}', (this.properties.isInternal ? 'internal/' : ''))
          .replace('#{trnReference}', this.trnReference.replace(/#/g, '%23') ? this.trnReference.replace(/#/g, '%23') : '')
          .replace('#{reportSpace}', this.properties.ReportSpace ? this.properties.ReportSpace : 'SARB')
          .replace('#{channelName}', this.properties.ReportChannel ? this.properties.ReportChannel : 'coreSARB')
        );
        // this.baseUrl + 'internal/producer/api/report/documents?channelName=invBMP&trnReference=' + this.trnReference.replace(/#/g, '%23')

        return this.$http
            .get(getDocsRequiredEndpoint);
    }

}

export const upload: angular.IComponentOptions = {
    template: require('./upload.html'),
    controller: UpoadController
};
