import { authService } from '../services/authService/authService';
import { ConfigService } from '../services/ConfigService/configService';
import { PropertiesService } from '../services/propertiesService/propertiesService';
import { SSL_OP_SSLREF2_REUSE_CERT_TYPE_BUG } from 'constants';
import { LoaderService } from '../services/LoaderService/loaderService';
import { ReportSpaceService } from '../services/ReportingSpaceService/reportSpaceService';
import { RegulatorClass } from '../interfaces/RegulatorFields';

class DashboardController implements angular.IController {

  public reportSpace: string;
  public channel: string;
  public reportState: string;
  public currentReportList: string = 'All';
  public reports;
  public searchText = '';
  public toggleMap;
  public responseStatus = '';
  public lists = [];
  public headers = [];
  public fields = [];
  public reportSpaces;
	public maxRecordPageAmount: number;
	public currentPage: number = 1;
	public pageNumbers: string = '';
	public recordCount: number = 0;
	public fieldDisplay;

	private properties;

	constructor(
		private authService: authService,
		private LoaderService: LoaderService,
		private $http: angular.IHttpService,
		private ConfigService: ConfigService,
		private $state: ng.ui.IStateService,
		private ReportSpaceService: ReportSpaceService) {
	
			var _self = this;
			this.properties = PropertiesService.getProperties();
			_self.LoaderService.showLoader();
			authService.whoAmI().then(function () {
				_self.reportSpace = ReportSpaceService.getReportSpace();
				_self.channel = ReportSpaceService.getChannel();
				_self.getListsNames();
			}).then(function () {
				_self.getReports(_self.currentReportList);
				_self.reportSpaces = _self.ReportSpaceService.getAllReportSpaces();
			});
		}

	public getListsNames() : void {
	var url = '';
	var _self = this;

	url = _self.properties.ReportServicesBaseUrl +
		(
		_self.properties.ReportServicesListsUrl
		.replace('#{internal}', (this.properties.isInternal ? 'internal/' : ''))
		);

	this.$http
		.get(url)
		.then((response: any) => {
			_self.lists = response.data;
			_self.responseStatus = response.status.toString();
			if (_self.lists) {
				var tempList = _self.lists;
				_self.lists.sort((a, b) => a.localeCompare(b));
				_self.currentReportList = _self.lists[0];
			}
		}, (error) => {
			_self.$state.transitionTo('errorPage');
		});
	}

	public getPageAmount(reportSpace : string) {

		if (!this.fieldDisplay) {
			return 100;
		}

		var data = this.fieldDisplay['config'];
		for (var regCount in data) {
			if (data[regCount].regulator === reportSpace) {
        return data[regCount].rowCount;
      }
    }
	}

	public async getConfigData() {
		let url  = this.properties.ReportServicesBaseUrl + (this.properties.ReportServicesColumnConfig.replace('#{internal}', ('internal/'))
		.replace('#{name}', 'rsuiColumnDisplay'));
		let data = await this.$http.get(url).then((response: any) => {
			return response.data;
		});
		
    return data;
	}

	public async getConfigDisplay() {
		if (!this.fieldDisplay) {
			this.fieldDisplay = await this.getConfigData();
		}
		this.maxRecordPageAmount = await this.getPageAmount(this.reportSpace);
		let startNumber = this.currentPage * this.maxRecordPageAmount - this.maxRecordPageAmount + 1;
		// console.log("This is amount: ", (startNumber + (this.maxRecordPageAmount - 1)))
		this.pageNumbers = 'Showing ' + startNumber.toString() + ' - ' + (startNumber + (this.maxRecordPageAmount - 1)).toString();
	}

	public getReportListColumns() {
	var url = '';
	var _self = this;
	url = _self.properties.ReportServicesBaseUrl + (
		_self.properties.ReportServicesListsAll
		.replace('#{internal}', (this.properties.isInternal ? 'internal/' : ''))
	);

	url = url.replace('{listName}', '' + this.currentReportList);
	this.$http.get(url).then((response: any) => {
			_self.fields = response.data.fields;
	},
	(error) => {
		_self.$state.transitionTo('errorPage');
	});
	}

	public async getReports(reportList: string, page?: number) {

		await this.getConfigDisplay();

		this.LoaderService.showLoader();
		this.reportSpace = this.ReportSpaceService.getReportSpace();
		this.channel = this.ReportSpaceService.getChannel();

		if (reportList !== 'Searching') {
			this.currentReportList = reportList;
		}
		var url = '';
		var _self = this;
		_self.getReportListColumns();
		url = _self.properties.ReportServicesBaseUrl +
			(
			_self.properties.ReportServicesListUrl
			.replace('#{internal}', (this.properties.isInternal ? 'internal/' : ''))
			.replace('#{list}', this.currentReportList)
			.replace('#{reportSpace}', this.reportSpace ? this.reportSpace : 'SARB')
			.replace('#{channelName}', this.channel ? this.channel : 'coreSARB')
			.replace('#{maxRecords}', this.maxRecordPageAmount.toString() ? this.maxRecordPageAmount.toString() : '100' )
			.replace('#{search}', (_self.searchText === '' ? '' : '&search=' + _self.searchText))
			.replace('#{pageNumber}', (page ? page : 1))
			);

		this.$http.get(url).then((response: any) => {
			_self.headers = response.data.Headers;

			if (response.data.Rows) {
				for (var i = 0; i < response.data.Rows.length; i++) {
					for (var fieldIdx = 0; fieldIdx < this.fields.length; fieldIdx++) {
						if (this.fields[fieldIdx].label !== this.fields[fieldIdx].name && response.data.Rows[i][this.fields[fieldIdx].label]) {
							response.data.Rows[i][this.fields[fieldIdx].name] = response.data.Rows[i][this.fields[fieldIdx].label];
						}
					}
				}
			}

			_self.reports = response.data.Rows;
			let startNumber = 	_self.currentPage * 	_self.maxRecordPageAmount - 	_self.maxRecordPageAmount + 1;
			_self.recordCount = response.data.Rows.length;
			_self.pageNumbers = 'Showing ' + startNumber.toString() + ' - ' + (startNumber + 	_self.recordCount - 1).toString();
			_self.responseStatus = response.status.toString();
			// tslint:disable-next-line:typedef
			_self.toggleMap = _self.reports.reduce(function (memo, item) {
				memo.push({
					Reference: item.Reference,
					isToggled: false
				});
				return memo;
			}, []);
			_self.LoaderService.hideLoader();
		}, (error) => {
			console.log('Unable to fetch list data for list: ' + reportList + ' => ' + url, error );
			_self.$state.transitionTo('errorPage');
			// _self.LoaderService.hideLoader();
		});
	}

	public toggle(Reference: String, Status: String) {

		// tslint:disable-next-line:typedef
		this.toggleMap.map(function (item) {
				if (item.Reference !== Reference) {
				item.isToggled = false;
			}
		});
		// tslint:disable-next-line:typedef
		var ref = this.toggleMap.find(function (i) {
			return i.Reference === Reference;
		});
		if (ref.isToggled) {
			ref.isToggled = false;
		} else {
			ref.isToggled = true;
		}
	}

	public isToggled(Reference: String): Boolean {
		// tslint:disable-next-line:typedef
		var ref = this.toggleMap.find(function (i) {
			return i.Reference === Reference;
		});
		return ref.isToggled;
	}

	public getButton(butName : string, report : any) : boolean {
		return this.ConfigService.getConfigs(this.reportSpace.toString(), report, butName);
	}

  // tslint:disable-next-line:typedef
	public getFlagClass(currency) {
	var curr = currency.toLowerCase();
	var lookupArray = [
		'sa', 'zar', 'zaf', 'za', 'uk', 'gbp', 'gb', 'usa', 'usd', 'us',
		'eu', 'eur', 'es', 'aus', 'aud', 'au', 'sw', 'chf', 'ch',
		'can', 'cad', 'ca', 'dnk', 'dkk', 'dk', 'hk', 'hkd', 'hkg',
		'jp', 'jpy', 'jpn', 'mu', 'mur', 'mus', 'nz', 'nzd', 'nzl',
		'no', 'nok', 'nor', 'il', 'ils', 'isr', 'sg', 'sgd', 'sgp',
		'se', 'sek', 'swe', 'cn', 'cny', 'chn', 'bw', 'bwp', 'bwa',
		'sk', 'svk', 'hu', 'huf', 'hun', 'ke', 'kes', 'ken',
		'mw', 'mwk', 'mwi', 'mx', 'mxn', 'mex', 'my', 'myr', 'mys',
		'ae', 'aed', 'are', 'zm', 'zmw', 'zmb', 'th', 'thb', 'tha',
		'pl', 'pln', 'pol', 'tr', 'try', 'tur', 'ugx', 'ug',
		'tzs', 'tz', 'ghs', 'gh', 'ngn', 'ng', 'cu', 'czk'
	];
	if (lookupArray.indexOf(curr) !== -1) {
		return curr;
	} else {
		return 'blank';
	}
	}

	public showHeader(heading : any) : boolean {
		return this.getHeaders(this.reportSpace.toString(), this.currentReportList.toString(), heading.toString());
	}

	public getHeaders(reportSpace : string, listType : string, columnName : string) : boolean {
		if (!this.fieldDisplay)	{
			return true;
		}
		var data = this.fieldDisplay['config'];
		for (var regCount in data) {
			if (data[regCount].regulator == reportSpace) {
				for (var listCount in data[regCount].lists) {
					if (data[regCount].lists[listCount].listName == listType) {
						for (var fieldCounter in data[regCount].lists[listCount].fields) {
							if (data[regCount].lists[listCount].fields[fieldCounter].fieldName == columnName) {
								return data[regCount].lists[listCount].fields[fieldCounter].shouldDisplay;
							}
						}
					}
				}
			}
		}

		
		/* tslint:enable:no-string-literal */
		// const tempObj = columnConfigs[reportSpace][listType];
		// var listConfig = Object.keys(tempObj).map((key) => {
		// 	return { key: key, value: tempObj[key] };
		// });

		// for (var i = 0; i < listConfig.length; i++) {
		// 	if (Array.isArray(listConfig[i].value)) {
		// 		if ((listConfig[i].value).includes(columnName)) {
		// 			return false;
		// 		}
		// 	} else if (columnName.toLowerCase() === listConfig[i].value.toLowerCase()) {
		// 		return false;
		// 	}
		// }
		return true;
	}

	public changeChannel(channel : string) {
		var tempSplitter = channel.split(' : ');

		this.ReportSpaceService.setChannel(tempSplitter[1]);
		this.ReportSpaceService.setReportSpace(tempSplitter[0]);

		this.getReports(this.currentReportList);
	}

	public nextPage() {
		this.currentPage += 1;
		this.getReports(this.currentReportList, this.currentPage);
	}

	public previousPage() {
		this.currentPage -= 1;
		this.getReports(this.currentReportList, this.currentPage);
	}

}



export const dashboard: angular.IComponentOptions = {
  template: require('./dashboard.html'),
  controller: DashboardController,
  controllerAs: 'dashboard'
};
