var express = require('express');
var proxy = require('proxy-middleware');
var url = require('url');

var proxyUrl = 'http://localhost:81/report-data-store';
//var proxyUrl = 'http://ec2-34-242-236-95.eu-west-1.compute.amazonaws.com:8080/report-data-store';
var proxyRoute = "/report-data-store/";


var app = express();
var port = 3002;



var proxyOptions = url.parse(proxyUrl);
proxyOptions.auth = "superuser:abc";

app.use(proxyRoute, proxy(proxyOptions));

// app.use(express.static(__dirname + '/.tmpAdmin'));

	// DEV
app.use(express.static(__dirname + '/.tmp'));

	// PROD
 //app.use(express.static(__dirname + '/dist'));


app.get('*', function(req, res) {
	// res.sendFile(__dirname+ '/.tmpAdmin/index.html');
	
	// DEV
	res.sendFile(__dirname+ '/.tmp/index.html');
	
	// PROD
	 //res.sendFile(__dirname + '/dist/index.html');
	
});


app.listen(port, () => console.log('app listening on port ' + port));

exports = module.exports = app;