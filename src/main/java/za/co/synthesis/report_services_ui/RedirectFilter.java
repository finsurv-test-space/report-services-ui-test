package za.co.synthesis.report_services_ui;

// import com.sun.org.apache.regexp.internal.REUtil;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Arrays;

/**
 * For the One-Page app, certain app paths always need to return content specified in the
 * redirect target resource. Otherwise we continue processing with the servlet chain.
 * The filter should be initialised with 2 variables
 * <br>
 * <br>
 * <b>paths:</b> semicolon delimited strings representing the path patterns to match for example: "/;/app/*;/login"
 * <br>
 * <b>redirectTarget:</b> target resource wich will serve the content for this response e.g: "/index.jsp"
 * <br>
 * Created by Marais Neethling on 2017/10/19.
 */

public class RedirectFilter implements Filter {

  private String redirectTarget;
  private String[] exactMatches;
  private String[] patternMatches;

  @Override
  public void init(FilterConfig filterConfig) {
    redirectTarget = filterConfig.getInitParameter("redirectTarget");
    exactMatches = extractExactMatchPaths(filterConfig.getInitParameter("paths"));
    patternMatches = extractPatternMatchPaths(filterConfig.getInitParameter("paths"));
  }

  private String[] extractPatternMatchPaths(String paths) {
    return Arrays.stream(paths
            .split(";"))
            .filter(s -> s.endsWith("*"))
            .map(s -> s.substring(0,s.indexOf("*")))
            .toArray(String[]::new);
  }

  private String[] extractExactMatchPaths(String paths) {
    return Arrays.stream(paths
            .split(";"))
            .filter(s -> !s.endsWith("*"))
            .toArray(String[]::new);
  }

  @Override
  public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
    HttpServletRequest req = (HttpServletRequest) servletRequest;
    String path = req.getRequestURI().substring(req.getContextPath().length());
    if (exactMatch(path,exactMatches) || startsWithMatch(path,patternMatches)) {
      servletRequest.getRequestDispatcher(redirectTarget).forward(servletRequest, servletResponse);
    } else {
      filterChain.doFilter(servletRequest, servletResponse); //Process normal servlet chain
    }
  }

  private boolean exactMatch(String pattern, String[] toMatch) {
    if (pattern == null) return false;
    return Arrays.stream(toMatch).anyMatch(s -> pattern.equals(s));
  }

  private boolean startsWithMatch(String pattern, String[] toMatch) {
    if (pattern == null) return false;
    return Arrays.stream(toMatch).anyMatch(s -> pattern.startsWith(s));
  }

  @Override
  public void destroy() {

  }
}
