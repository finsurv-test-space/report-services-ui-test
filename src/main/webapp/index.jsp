<%--
  Created by IntelliJ IDEA.
  User: marais
  Date: 2017/10/21
  Time: 5:48 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html id="patternlab-html">
<head>
    <base href="<%=request.getContextPath()%>/">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Report Services 6.6.6</title>
    <meta name="description" content="">
    <meta lang="en">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png"  />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body ng-app="app">
    <ui-view></ui-view>
    <script type="text/javascript" src="index.js"></script>
</body>
</html>

