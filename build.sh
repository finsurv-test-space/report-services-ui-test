#!/bin/bash

set -eE

ROOT=$PWD

if [ $# -gt 0 ]
then
  echo "Version is set to $1"
  sed -i "s|^\s*version =.*|version = \"$1\"|g" ./build.gradle
  sed -i "s|^\s*\"version\":.*|\"version\": \"$1\",|g" ./web/properties.json
  sed -i "s|^\s*<title>Report Services.*</title>|<title>Report Services $1</title>|g" ./web/index.html
  sed -i "s|^\s*<title>Report Services.*</title>|<title>Report Services $1</title>|g" ./distAdmin/index.html
  sed -i "s|^\s*<title>Report Services.*</title>|<title>Report Services $1</title>|g" ./src/main/webapp/index.jsp
fi

write_metadata(){
   echo "{
        	\"artefact\":
                [{
                	\"name\":\"${ROOT}/build/libs/*.war\",
                	\"destination\":\"TOMCAT_FRONT\"
                }]
        }" >> ${ROOT}/build/metadata.json
}

rm -rf build

./gradlew clean
npm install
npm run build
./gradlew wars

write_metadata

# if [ ! -d "BuiltFiles" ]
# then
# 	mkdir "BuiltFiles"
# fi

# rm -f "BuiltFiles/*"
# cp "build/libs/"*.war "BuiltFiles/"